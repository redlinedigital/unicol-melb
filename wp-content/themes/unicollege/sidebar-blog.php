<?php
if ( is_active_sidebar( 'sidebar-blog' ) ) :
    echo '<aside class="sidebar aside">';
        dynamic_sidebar( 'sidebar-blog' );
    echo '</aside>';
endif; 