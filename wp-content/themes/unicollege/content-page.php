
<div class="container">
	<div class="columns">
	<?php if( have_rows('photos') ): ?>
		<div class="column">
	<?php else: ?>
		<div class="column is-10">
	<?php endif; ?>


	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

		<?php
		$pagetype = get_post_type();
		?>

		<?php
		// STUDENTS PAGE

		if( $pagetype !== 'event' ): ?>
			<header class="entry-header">
				<h1 class="entry-title museo-slab-300 blue"><?php the_title(); ?></h1>
			</header>
		<?php endif; ?>

		<div class="entry-content">
			<?php the_content(); ?>


			<?php
			// FELLOWS PAGE

			if( have_rows('fellows') ): ?>
				<?php while( have_rows('fellows') ): the_row();
					// vars
					$photo = get_sub_field('photo');
					$name = get_sub_field('name');
					$role = get_sub_field('role');
					$description = get_sub_field('description');
					?>
					<div class="fellows">
						<div class="columns">
							<div class="column is-3">
								<figure class="image is-square">
									<img src="<?php echo $photo['url']; ?>" alt="<?php echo $photo['alt'] ?>" />
								</figure>
							</div>
							<div class="column">
								<hr class="small"/>
								<h3 class="museo-slab-300 blue-darker"><?php echo $name; ?></h3>
								<?php if( $role ): ?>
									<p>
										<span class="museo-slab-700 blue"><?php echo strtoupper($role); ?></span>
									</p>
								<?php endif; ?>
								<?php echo $description; ?>
							</div>
						</div>
					</div>
				<?php endwhile; ?>
			<?php endif; ?>


			<?php
			// STUDENTS PAGE
			if( have_rows('students') ): ?>
				<?php while( have_rows('students') ): the_row();
					// vars
					$photo = get_sub_field('photo');
					$name = get_sub_field('name');
					$role = get_sub_field('role');
					$description = get_sub_field('description');
					$page = get_sub_field('page');
					?>
					<div class="students">
						<div class="columns">
							<div class="column is-3">
								<figure class="image is-square">
									<img src="<?php echo $photo['url']; ?>" alt="<?php echo $photo['alt'] ?>" />
								</figure>
							</div>
							<div class="column">
								<hr class="small"/>
								<h3 class="museo-slab-300 blue-darker"><?php echo $name; ?></h3>
								<?php if( $role ): ?>
									<p>
										<span class="museo-slab-700 blue"><?php echo strtoupper($role); ?></span>
									</p>
								<?php endif; ?>
								<?php echo $description; ?>
								<?php if( $page ): ?>
									<a href="<?php echo $page; ?>" class="button is-info">
										READ MORE <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
									</a>
								<?php endif; ?>
							</div>
						</div>
					</div>
				<?php endwhile; ?>
			<?php endif; ?>


			<?php
			// GALLERY PAGE
			if( have_rows('photos') ): ?>

			<div class="gallery">
				<?php while( have_rows('photos') ): the_row();
					// vars
					$photo = get_sub_field('photo');
					$size = get_sub_field('size');
					if(!$size) $size = 'size11';
					?>
					 <a href="<?php echo $photo['url']; ?>" data-title="<?php echo $photo['description'] ?>" data-lightbox="gallery" class="grid-item <?php echo $size; ?>">
						 <?php if($photo['description']): ?>
						 <div class="overlay">
								 <span class="has-text-centered"><?php echo $photo['description'] ?></span>
						 </div>
					 	<?php endif; ?>
						<figure class="image is-square">
							<img src="<?php echo $photo['sizes']['gallery-' . $size]; ?>" alt="<?php echo $photo['alt'] ?>"/>
						</figure>
					</a>
				<?php endwhile; ?>
			</div>
			<?php endif; ?>
		</div><!-- .entry-content -->

		<!-- <footer class="entry-meta"> -->
			<?php //edit_post_link( __( 'Edit Page'), '<span class="edit-link">', '</span>' ); ?>
		<!-- </footer> -->
	</article><!-- #post-<?php the_ID(); ?> -->
</div>
</div>
</div>
