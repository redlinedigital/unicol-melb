<?php
/**
 * The template for displaying 404 pages (Not Found)
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

 get_header(); ?>
 <div class="content-wrap">
 	<div class="content">
 		<section class="main content-page">
      <div class="container">
      	<div class="columns">
    			<div class="column">
          	<article id="post-220" class="post-220 page type-page status-publish hentry has-text-centered">
    					<header class="entry-header">
        				<h1 class="entry-title museo-slab-300 blue">Page Not Found</h1>
        			</header>
          		<div class="entry-content">
          			<p class="style-1">We can't seem to find the page that you are looking for.</p>
                <p>If you can't find what your looking for, please contact us.</p>
    					</div>
          	</article>
          </div>
        </div>
      </div>
 		</section>
 	</div>
 </div>
 <?php get_footer(); ?>
