# global module:false
module.exports = (grunt) ->
  grunt.registerTask 'hello', ->
    console.log "Hello from the Gruntfile!"

  'use strict'

  # Project configuration.
  grunt.initConfig({
    pkg: require('./package')

    clean:
      js: ['js/app.js', 'js/app.min.js', 'js/app.concat.js']
      css: ['editor-style.css']


    # Compile Coffee
    coffee:
      compile:
        files:
          'js/app.js': 'coffee/app.coffee'

    # Concat Vendor files
    concat:
      dist:
        src: [
          'node_modules/magnific-popup/dist/jquery.magnific-popup.min.js',
          'node_modules/lightbox2/dist/js/lightbox.min.js',
          'node_modules/isotope-layout/dist/isotope.pkgd.min.js',
          'node_modules/isotope-packery/packery-mode.pkgd.min.js',
          'node_modules/slick-carousel/slick/slick.min.js',
          'js/vendor/*.js',
          'js/app.js'
        ]
        dest: 'js/app.concat.js'


    # Minifiy Vendor files
    uglify:
      dist:
        files:
          'js/app.min.js': 'js/app.concat.js'

    # Watching Sass and Coffe file changes
    watch:
      gruntfile:
        files: 'Gruntfile.coffee'
      coffee:
        files: ['coffee/*.coffee']
        tasks: ['coffee']
      concat:
        files: ['js/vendor/*.js', 'js/app.js']
        tasks: ['concat']
      uglify:
        files: ['js/app.concat.js']
        tasks: ['uglify']

    # Notifications
    notify:
      watch:
        options:
          title: 'Task Complete'
          message: 'Sass and Coffee files compiled'

    browserSync:
      dev:
        bsFiles:
          src: [
            './**/*.php',
            './**/*.css',
            './**/*.js'
          ] # Exclude map files
        options:
          open: 'external'
          host: 'unicolmelb.localhost'
          watchTask: true
          proxy: 'unicolmelb.localhost'
  })

  grunt.loadNpmTasks('grunt-contrib-coffee')
  grunt.loadNpmTasks('grunt-contrib-watch')
  grunt.loadNpmTasks('grunt-contrib-concat')
  grunt.loadNpmTasks('grunt-contrib-uglify')
  grunt.loadNpmTasks('grunt-browser-sync');
  grunt.loadNpmTasks('grunt-notify')
  grunt.loadNpmTasks('grunt-contrib-clean');

  grunt.registerTask('scripts', [
    'coffee'
    , 'concat'
    , 'uglify'
  ])

  grunt.registerTask('default', [
    'clean'
  , 'coffee'
  , 'concat'
  , 'uglify'
  , 'notify'
  , 'browserSync'
  , 'watch'
  ])