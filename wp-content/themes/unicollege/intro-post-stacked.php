<?php
/**
 * The Template for displaying a post
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
?>
<article id="post-<?php the_ID(); ?>" class="intro-post">
    <figure class="image is-square">
        <?php
        if ( has_post_thumbnail() ) {
            the_post_thumbnail( array(347, 347) );
        }
        ?>
    </figure>
    <hr class="small"/>
    <a href="<?php echo get_permalink(); ?>" class="intro-post__title museo-slab-300 blue-darker">
        <?php the_title(); ?>
    </a>
    <?php the_excerpt(); ?>
    <div>
        <a href="<?php echo get_permalink(); ?>" class="button is-info">
            READ ARTICLE <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
        </a>
    </div>
</article>
