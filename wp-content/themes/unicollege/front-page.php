<?php
get_header();

get_template_part( 'template-parts/homepage/hero' );
get_template_part( 'template-parts/homepage/welcome' );
get_template_part( 'template-parts/homepage/features' );
get_template_part( 'template-parts/homepage/videos' );
get_template_part( 'template-parts/homepage/sections' );
get_template_part( 'template-parts/homepage/visit' );

/**
 * Pixel track
 * https://basecamp.com/1971229/projects/11025518/todos/255464604
 */
 ?>
<img src='https://s.tribalfusion.com/i.cid?c=696993&d=30&page=landingPage' width='1' height='1' border='0' style="width:0;height:0;position:absolute">

<?php
get_footer();