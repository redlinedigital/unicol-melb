<?php get_header(); ?>
<div class="content-wrap">
	<div class="content">
        <?php
        get_template_part( 'breadcrumb' );
        ?>

        <div class="container content-sidebar-wrap">
            <section class="main content-page">
                    <header class="entry-header">
                        <h1 class="entry-title museo-slab-300 blue">
                            <?php
                            $current_category = single_cat_title("", false);
                            if($current_category != null) echo $current_category;
                            else single_post_title(); ?>
                        </h1>
                    </header>
                    <?php
                    if(!$current_category)
                        the_field('introduction', 59); ?>

                    <?php
                        while ( have_posts() ) : the_post();
                            get_template_part( 'intro', get_post_type() );
                        endwhile; // end of the loop.
                    ?>


                    <?php
                    $prev_link = get_previous_posts_page_link();
                    $next_link = get_next_posts_page_link();
                    ?>


                    <nav class="pagination">
                        <?php if($paged > 1): ?>
                        <a href="<?php echo $prev_link; ?>" class="button">Newer posts</a>
                        <?php endif; ?>

                        <?php if($wp_query->max_num_pages > $paged): ?>
                            <a href="<?php echo $next_link; ?>" class="button">Older posts</a>
                        <?php endif; ?>
                    </nav>
            </section>

            <?php get_sidebar( 'blog' ); ?>
            
	    </div>
	</div>
</div>
<?php get_footer(); ?>
