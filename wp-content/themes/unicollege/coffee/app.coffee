jQuery(document).ready ($) ->
  $toggle              = $('#nav-toggle')
  $menu                = $('#nav-menu')
  $parents_menu_mobile = $('.parent-icon', $menu)
  $donation            = $('a.donation')
  $give_direction      = $('input[name=give_direction]')
  $popup_iframe        = $('.popup-iframe')
  $slick_slider        = $('.slick-slider')

  # magnific popup
  $popup_iframe.magnificPopup
      type: 'iframe'

  # slick carousel
  $slick_slider.slick
        autoplay: false,
        arrows: true,
        dots: true

  # homepage video in slider
  $('.homepage-hero').on('beforeChange', (slick, currentSlide, nextSlide) ->
    current_slide  = currentSlide.currentSlide
    $current_video = $(currentSlide.$slides[current_slide]).find('video')
    if($current_video.length > 0)
        $current_video.get(0).pause()
  )

  $('.homepage-hero').on('afterChange', (slick, currentSlide) ->
    current_slide  = currentSlide.currentSlide
    $current_video = $(currentSlide.$slides[current_slide]).find('video')
    if($current_video.length > 0)
      $current_video.get(0).play()
  )

  $('.inscrition_chair, .syme_chair').hide()
  $('.student_scholarships').hide()

  $give_direction.change (e, a) ->
    # console.debug(e, a)
    if e.target.value == "Syme Dining Hall Chair Fund"
      $('.inscrition_chair, .syme_chair').show()
    else
      $('.inscrition_chair, .syme_chair').hide()
    if e.target.value == "Student Scholarships"
      $('.student_scholarships').show()
    else
      $('.student_scholarships').hide()

  $toggle.click ->
    $(this).toggleClass 'is-active'
    $menu.toggleClass 'is-active'

  $parents_menu_mobile.click ->
    $(this).find('.fa').toggleClass('fa-chevron-down')
    $(this).find('.fa').toggleClass('fa-chevron-up')
    $(this).next().toggleClass('is-open')

  $donation.click ->
    $(this).find('.heart').addClass('is-animating')

  # remove the input from the span on contact-form-7
  $('.wpcf7-form-control-wrap').each ->
    el = $('select, input, textarea', this)
    pa = $(this).parent().find('label')
    el.insertBefore(pa)


  # Animation for inputs
  markHasValue = (x) ->
    if ($(x).val())
      $(x).addClass('has-value');
    else
      $(x).removeClass('has-value');

  $('input, textarea').blur ->
    markHasValue(this)
  $('input, textarea').each ->
    markHasValue(this)


  # Animation for search form
  $('.search-title').click (e) ->
    e.preventDefault()
    form = $(this).next('form')
    form.addClass('is-open')
    form.find('.search-input').focus()
  $('.search-input').blur ->
    $(this).parents('form').removeClass('is-open')


  # Gallery organization
  $('.gallery').on( 'arrangeComplete', ->
      $(this).addClass('is-visible')
  ).isotope
    layoutMode: 'packery',
    itemSelector: '.grid-item'
    packery:
      gutter: 2
    # masonry:
    #   columnWidth: '.grid-sizer'


  # Modal
  $('.modal-button').click ->
    target = $(this).data('target')
    $('html').addClass 'is-clipped'
    $(target).addClass 'is-active'

  $('.modal-background, .modal-close').click ->
    $('html').removeClass 'is-clipped'
    $(this).parent().removeClass 'is-active'


  # Style Support US
  # 2021-06-02 Redline removed due to this breaking the form
#   supportform = $('.give-form')
#   if(supportform)
#     $('.give-total-wrap, .give-donation-levels-wrap').addClass('columns')
#     $('.give-donation-amount, .give-donation-levels-wrap li').addClass('column')
#     $('.give-donation-levels-wrap').addClass('form-radio')
#     $('.give-donation-levels-wrap li').addClass('radio')
#     $('.give-donation-levels-wrap input').each ->
#       t = $(this)
#       n = t.next()
#       t.detach().prependTo(n)
#       $(this).after($('<i class="helper"></i>'))
#     # $('#give-payment-mode-select').hide()
#     inputs = supportform.find('#give_purchase_form_wrap').find('input[type=text], input[type=tel], input[type=email], select')
#     inputs.each ->
#       bar = $('<i class="bar"></i>')
#       t = $(this)
#       t.after(bar)
#       p = t.parent()
#       p.addClass('form-group')
#       t.detach().prependTo(p)
#       p.find('label').addClass('control-label')
#       t.removeAttr('placeholder')
#     $('.give-submit').addClass('button').addClass('is-info')
#     inputs.promise().done ->
#       supportform.fadeIn("slow")
#   $('.give-table').addClass('table')

  $('#institution-attended-other').hide()
  $('input[type=radio][name=institution-attended]').change ->
    if(this.value == 'Other')
      $('#institution-attended-other').show()
    else
      $('#institution-attended-other').hide()

  $('#other-interest').hide()
  $('input[type=checkbox][name=interested-in\\[\\]]').change ->
    if($('input[type=checkbox][name=interested-in\\[\\]][value=Other]:checked').length)
      $('#other-interest').show()
    else
      $('#other-interest').hide()
    
  # Venue template stuff
  $('.venues-list-item h3').click ->
    $(this).parent().toggleClass('open') 

  

  $(window).scroll ->
    $(".tile").find('img').each ->
      if($(this).visible(true))
        $datasrc = $(this).data('src')
        $(this).attr('src',$datasrc)
    
    $(".featured-item").find('img').each ->
      if($(this).visible(true))
        $datasrc = $(this).data('src')
        $(this).attr('src',$datasrc)

  return