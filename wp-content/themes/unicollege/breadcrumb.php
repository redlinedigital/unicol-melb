<?php
/**
 */
?>

<section class="breadcrumb is-hidden-mobile no-print">
	<div class="container">
		<div class="columns">
			<div class="column breadcrumbs museo-slab-300 blue-darker" typeof="BreadcrumbList" vocab="http://schema.org/">
					<?php if(function_exists('bcn_display'))
					{
							bcn_display();
					}?>
			</div>
			<div class="column is-2 has-text-right">
				<?php if ( function_exists( 'ADDTOANY_SHARE_SAVE_KIT' ) ) { ADDTOANY_SHARE_SAVE_KIT(); } ?>
			</div>
		</div>
	</div>
</section>
