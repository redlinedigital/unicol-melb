<?php
// latest posts
// show on homepage and /future-students/*
if ( is_front_page() || is_tree( 476 ) ) {
    get_template_part( 'template-parts/latest-posts' );
}
?>

    <section class="footer">
        <div class="full-columns is-gapless ivy-bg no-print">
            <div class="full-column">
                <a href="<?php the_field('footer_facebook', 'option'); ?>"
                    class="social-button" target="_blank">
                    <div class="columns is-mobile is-gapless">
                        <div class="column social-button-text">
                            <i class="fa fa-facebook-square" aria-hidden="true"></i>
                            Find us on facebook
                        </div>
                        <div class="column is-narrow social-button-arrow">
                            <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                        </div>
                    </div>
                </a>
            </div>


            <div class="full-column">
                <a href="https://www.linkedin.com/company/ucmelbourne"
                    class="social-button" target="_blank">
                    <div class="columns is-mobile is-gapless">
                        <div class="column social-button-text">
                            <i class="fa fa-linkedin-square" aria-hidden="true"></i>
                            Follow us on linkedin
                        </div>
                        <div class="column is-narrow social-button-arrow">
                            <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                        </div>
                    </div>
                </a>
            </div>
        </div>

        <footer class="footer">
            <div class="container">
                <div class="columns">
                    <?php the_footer_menu(); ?>
                    <div class="column is-half">
                        <?php if( get_field('footer_phone', 'option') ): ?>
                            <p class="option-name"><i class="fa fa-phone" aria-hidden="true"></i> Phone</p>
                            <p class="option-value"><?php the_field('footer_phone', 'option'); ?></p>
                        <?php endif; ?>
                        <p>&nbsp;</p>
                        <?php if( get_field('footer_location', 'option') ): ?>
                            <p class="option-name"><i class="fa fa-map-marker" aria-hidden="true"></i> Location</p>
                            <p class="option-value"><?php the_field('footer_location', 'option'); ?></p>
                            <a href="<?php the_field('footer_location_page', 'option'); ?>" class="no-print">view on map ></a>
                        <?php endif; ?>
                        <p>&nbsp;</p>
                        <p class="option-value">University College acknowledges the Wurundjeri people of the Kulin Nation - who have been custodians of the land on which our college stands for thousands of years. We pay our respects to their Elders past, present and emerging.</p>
                    </div>
                </div>
                <?php if( get_field('footer_copyright', 'option') ): ?>
                <div class="columns">
                    <div class="column">
                            <span class="copyright"><?php the_field('footer_copyright', 'option'); ?></h2>
                    </div>
                </div>
                <?php endif; ?>
            </div>
        </footer>
        <?php wp_footer();?>
    </section>
    </body>
</html>
