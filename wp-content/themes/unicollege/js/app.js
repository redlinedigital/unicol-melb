(function() {
  jQuery(document).ready(function($) {
    var $donation, $give_direction, $menu, $parents_menu_mobile, $popup_iframe, $slick_slider, $toggle, markHasValue;
    $toggle = $('#nav-toggle');
    $menu = $('#nav-menu');
    $parents_menu_mobile = $('.parent-icon', $menu);
    $donation = $('a.donation');
    $give_direction = $('input[name=give_direction]');
    $popup_iframe = $('.popup-iframe');
    $slick_slider = $('.slick-slider');
    // magnific popup
    $popup_iframe.magnificPopup({
      type: 'iframe'
    });
    // slick carousel
    $slick_slider.slick({
      autoplay: false,
      arrows: true,
      dots: true
    });
    // homepage video in slider
    $('.homepage-hero').on('beforeChange', function(slick, currentSlide, nextSlide) {
      var $current_video, current_slide;
      current_slide = currentSlide.currentSlide;
      $current_video = $(currentSlide.$slides[current_slide]).find('video');
      if ($current_video.length > 0) {
        return $current_video.get(0).pause();
      }
    });
    $('.homepage-hero').on('afterChange', function(slick, currentSlide) {
      var $current_video, current_slide;
      current_slide = currentSlide.currentSlide;
      $current_video = $(currentSlide.$slides[current_slide]).find('video');
      if ($current_video.length > 0) {
        return $current_video.get(0).play();
      }
    });
    $('.inscrition_chair, .syme_chair').hide();
    $('.student_scholarships').hide();
    $give_direction.change(function(e, a) {
      // console.debug(e, a)
      if (e.target.value === "Syme Dining Hall Chair Fund") {
        $('.inscrition_chair, .syme_chair').show();
      } else {
        $('.inscrition_chair, .syme_chair').hide();
      }
      if (e.target.value === "Student Scholarships") {
        return $('.student_scholarships').show();
      } else {
        return $('.student_scholarships').hide();
      }
    });
    $toggle.click(function() {
      $(this).toggleClass('is-active');
      return $menu.toggleClass('is-active');
    });
    $parents_menu_mobile.click(function() {
      $(this).find('.fa').toggleClass('fa-chevron-down');
      $(this).find('.fa').toggleClass('fa-chevron-up');
      return $(this).next().toggleClass('is-open');
    });
    $donation.click(function() {
      return $(this).find('.heart').addClass('is-animating');
    });
    // remove the input from the span on contact-form-7
    $('.wpcf7-form-control-wrap').each(function() {
      var el, pa;
      el = $('select, input, textarea', this);
      pa = $(this).parent().find('label');
      return el.insertBefore(pa);
    });
    // Animation for inputs
    markHasValue = function(x) {
      if ($(x).val()) {
        return $(x).addClass('has-value');
      } else {
        return $(x).removeClass('has-value');
      }
    };
    $('input, textarea').blur(function() {
      return markHasValue(this);
    });
    $('input, textarea').each(function() {
      return markHasValue(this);
    });
    // Animation for search form
    $('.search-title').click(function(e) {
      var form;
      e.preventDefault();
      form = $(this).next('form');
      form.addClass('is-open');
      return form.find('.search-input').focus();
    });
    $('.search-input').blur(function() {
      return $(this).parents('form').removeClass('is-open');
    });
    // Gallery organization
    $('.gallery').on('arrangeComplete', function() {
      return $(this).addClass('is-visible');
    }).isotope({
      layoutMode: 'packery',
      itemSelector: '.grid-item',
      packery: {
        gutter: 2
      }
    });
    // masonry:
    //   columnWidth: '.grid-sizer'

    // Modal
    $('.modal-button').click(function() {
      var target;
      target = $(this).data('target');
      $('html').addClass('is-clipped');
      return $(target).addClass('is-active');
    });
    $('.modal-background, .modal-close').click(function() {
      $('html').removeClass('is-clipped');
      return $(this).parent().removeClass('is-active');
    });
    // Style Support US
    // 2021-06-02 Redline removed due to this breaking the form
    //   supportform = $('.give-form')
    //   if(supportform)
    //     $('.give-total-wrap, .give-donation-levels-wrap').addClass('columns')
    //     $('.give-donation-amount, .give-donation-levels-wrap li').addClass('column')
    //     $('.give-donation-levels-wrap').addClass('form-radio')
    //     $('.give-donation-levels-wrap li').addClass('radio')
    //     $('.give-donation-levels-wrap input').each ->
    //       t = $(this)
    //       n = t.next()
    //       t.detach().prependTo(n)
    //       $(this).after($('<i class="helper"></i>'))
    //     # $('#give-payment-mode-select').hide()
    //     inputs = supportform.find('#give_purchase_form_wrap').find('input[type=text], input[type=tel], input[type=email], select')
    //     inputs.each ->
    //       bar = $('<i class="bar"></i>')
    //       t = $(this)
    //       t.after(bar)
    //       p = t.parent()
    //       p.addClass('form-group')
    //       t.detach().prependTo(p)
    //       p.find('label').addClass('control-label')
    //       t.removeAttr('placeholder')
    //     $('.give-submit').addClass('button').addClass('is-info')
    //     inputs.promise().done ->
    //       supportform.fadeIn("slow")
    //   $('.give-table').addClass('table')
    $('#institution-attended-other').hide();
    $('input[type=radio][name=institution-attended]').change(function() {
      if (this.value === 'Other') {
        return $('#institution-attended-other').show();
      } else {
        return $('#institution-attended-other').hide();
      }
    });
    $('#other-interest').hide();
    $('input[type=checkbox][name=interested-in\\[\\]]').change(function() {
      if (($('input[type=checkbox][name=interested-in\\[\\]][value=Other]:checked').length)) {
        return $('#other-interest').show();
      } else {
        return $('#other-interest').hide();
      }
    });
    
    // Venue template stuff
    $('.venues-list-item h3').click(function() {
      return $(this).parent().toggleClass('open');
    });
    $(window).scroll(function() {
      $(".tile").find('img').each(function() {
        var $datasrc;
        if ($(this).visible(true)) {
          $datasrc = $(this).data('src');
          return $(this).attr('src', $datasrc);
        }
      });
      return $(".featured-item").find('img').each(function() {
        var $datasrc;
        if ($(this).visible(true)) {
          $datasrc = $(this).data('src');
          return $(this).attr('src', $datasrc);
        }
      });
    });
  });

}).call(this);
