<?php
/**
 * Hubink functions and definitions
 *
 * Set up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * When using a child theme you can override certain functions (those wrapped
 * in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before
 * the parent theme's file, so the child theme functions would be used.
 *
 * @link https://codex.wordpress.org/Theme_Development
 * @link https://codex.wordpress.org/Child_Themes
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are
 * instead attached to a filter or action hook.
 *
 * For more information on hooks, actions, and filters,
 * {@link https://codex.wordpress.org/Plugin_API}
 *
 * @package WordPress
 * @subpackage Hubink
 */


// Define some Globals
DEFINE('THEME', get_template_directory_uri());
DEFINE('IMG', THEME.'/img');
DEFINE('CSS', THEME.'/css');
DEFINE('JS', THEME.'/js');
DEFINE('WP', get_bloginfo('wpurl'));
DEFINE('SITE', get_bloginfo('url'));

/**
 * Set the content width based on the theme's design and stylesheet.
 *
 * @since Twenty Fifteen 1.0
 */
if ( ! isset( $content_width ) ) {
	$content_width = 660;
}

if ( ! function_exists( 'hubink_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 *
 * @since Twenty Fifteen 1.0
 */
function hubink_setup() {
	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * See: https://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 825, 510, true );
	add_image_size( 'gallery-size11', 149, 149, true );
	add_image_size( 'gallery-size22', 299, 299, true );
	add_image_size( 'gallery-size44', 599, 599, true );
	add_image_size( 'parent-page', 303, 304, true );
	add_image_size( 'galleries-page', 407, 408, true );
	add_image_size( 'publications', 305 );

	// This theme uses wp_nav_menu() in two locations.
	register_nav_menus( array(
		'primary_menu' => 'Primary Menu',
		'quick_menu'  => 'Quick Links Menu',
		'social_media_menu'  => 'Social Media Menu',
		'footer_menu'  => 'Footer Menu',
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'comment-form', 'comment-list', 'gallery', 'caption'
	) );

	if( function_exists('acf_add_options_page') ) {
	 	// add parent
		$parent = acf_add_options_page(array(
			'page_title' 	=> 'Theme General Settings',
			'menu_title' 	=> 'Theme Settings',
			'redirect' 		=> false
		));

		// add sub page
		acf_add_options_sub_page(array(
			'page_title' 	=> 'Footer Settings',
			'menu_title' 	=> 'Footer',
			'parent_slug' 	=> $parent['menu_slug'],
		));

	}


	# include the external files
	require_once 'include/_donation.php';
	require_once 'include/_shortcodes.php';
	require_once 'include/_menus.php';
	require_once 'include/_conditionals.php';
  require_once 'include/aq_resizer.php';

}
endif; // hubink_setup
add_action( 'after_setup_theme', 'hubink_setup' );


/*
 * Change the 'Posts' to the 'News' on admin page
 */
function revcon_change_post_label() {
		global $menu;
		global $submenu;
		$menu[5][0] = 'News';
		$submenu['edit.php'][5][0] = 'News';
		$submenu['edit.php'][10][0] = 'Add News';
		$submenu['edit.php'][16][0] = 'News Tags';
		echo '';
}
add_action( 'admin_menu', 'revcon_change_post_label' );

function revcon_change_post_object() {
		global $wp_post_types;
		$labels = &$wp_post_types['post']->labels;
		$labels->name = 'News';
		$labels->singular_name = 'News';
		$labels->add_new = 'Add News';
		$labels->add_new_item = 'Add News';
		$labels->edit_item = 'Edit News';
		$labels->new_item = 'News';
		$labels->view_item = 'View News';
		$labels->search_items = 'Search News';
		$labels->not_found = 'No News found';
		$labels->not_found_in_trash = 'No News found in Trash';
		$labels->all_items = 'All News';
		$labels->menu_name = 'News';
		$labels->name_admin_bar = 'News';
}
add_action( 'init', 'revcon_change_post_object' );


add_filter( 'the_content', 'wpse8170_add_custom_table_class' );
function wpse8170_add_custom_table_class( $content ) {
    return str_replace( '<table', '<table class="table"', $content );
}

/**
 * Enqueue scripts and styles.
 *
 * @since Twenty Fifteen 1.0
 */
function hubink_scripts() {
	// Load our main stylesheet.
    wp_enqueue_script('hubink-script2', JS . '/app.min.js#asyncload', array('jquery'), false, true);

  // avoid loading events scripts with jquery-ui dependancies if not needed:
  if (function_exists('is_events_related_page') && !is_events_related_page()) {
		wp_dequeue_script( 'events-manager' );
  }
}
add_action( 'wp_enqueue_scripts', 'hubink_scripts' );

function add_async_forscript($url)
{
    if (strpos($url, '#asyncload')===false)
        return $url;
    else if (is_admin())
        return str_replace('#asyncload', '', $url);
    else
        return str_replace('#asyncload', '', $url)."' async='async"; 
}
add_filter('clean_url', 'add_async_forscript', 11, 1);


function manage_wpcf7_assets() {
	// avoid loading contact form 7 dependancies if not on contact page:
	add_filter( 'wpcf7_load_js', '__return_false' );
	// and never load its default CSS:
	add_filter( 'wpcf7_load_css', '__return_false' );

	// add the scripts back in if on contact page:
	if ( function_exists( 'wpcf7_enqueue_scripts' ) && is_page(array('contact-us','make-an-enquiry')) ) {
		wpcf7_enqueue_scripts();
	}
}
add_action( 'wp', 'manage_wpcf7_assets' );


function manage_give_plugin_assets() {
	// TODO: Only removing these scripts from home page for now as
	// I don't know where this plugin is used. Feel free to improve.
	if (!is_page('support-uc')) {
		wp_dequeue_script('give');
	}
}
add_action( 'wp_enqueue_scripts', 'manage_give_plugin_assets' );

add_action('admin_head', 'my_custom_code');
function my_custom_code() {
  echo '<style>
    #editor .wp-block {
      max-width: 1600px !important;
    }
  </style>';
}



// This function will return true if we are looking at the page in question or one of its sub pages
// https://gist.github.com/intelliweb/5255966
function is_tree( $pid ) {      // $pid = The ID of the page we're looking for pages underneath
    global $post;               // load details about this page

	if ( is_page($pid) )
		return TRUE;            // we're at the page or at a sub page

	$anc = get_post_ancestors( $post->ID );
	foreach ( $anc as $ancestor ) {
		if( is_page() && $ancestor == $pid ) {
			return TRUE;
		}
	}

	return FALSE;  // we aren't at the page, and the page is not an ancestor
}


/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function rdln_widgets_init() {
	register_sidebar(
		array(
			'name'          => 'Blog Sidebar',
			'id'            => 'sidebar-blog',
			'description'   => '',
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}
add_action( 'widgets_init', 'rdln_widgets_init' );


/**
 * Functions taken from front-page.php
 */
function limitTextChars($content = false, $limit = false, $stripTags = false, $ellipsis = false) {
    if ($content && $limit) {
        $content  = ($stripTags ? strip_tags($content) : $content);
        $ellipsis = ($ellipsis ? "..." : $ellipsis);
        $content  = mb_strimwidth($content, 0, $limit, $ellipsis);
    }
    return $content;
}

function create_section_tile($item) {
	switch ($item['row_layout']) {
		case 'page':
			$tile = '<div class="tile is-parent">';
			$tile .= '<a class="tile" href="' . esc_url(get_permalink($item['page'])) . '" title="' . $item['page']->post_title . '">';
			$tile .= '<div class="tile is-child ' . $item['colour'] . '">';

			// image
			if($item['image']){

				// Generate the image (once) ensuring the original isn't being used.
				// This ensures it is properly optimised.
				// aq_resize( $url, $width, $height, $crop, $single, $upscale )
				list($item_image) = aq_resize( $item['image']['url'], 500, 500, false, false, true );

				$tile .= '<img data-src="' . $item_image . '"/>';
			}

			$tile .= '<article class="page">';

			// event dates

			if (function_exists('em_get_event')) :
				$event_data = em_get_event( $item['page']->ID, 'post_id' );

				if($event_data->event_id) {
					$event_start_date = strtotime($event_data->event_start_date);
					$event_end_date = strtotime($event_data->event_end_date);
					$tile .= '<p class="sub-title date-event">';
					$tile .= date_i18n(get_option('dbem_date_format'), $event_start_date);
					if($event_end_date) $tile .= ' - ' . date_i18n(get_option('dbem_date_format'), $event_end_date);
					$tile .= '</p>';
				}
			endif;

			$tile .= '<h3 class="title">' . $item['page']->post_title . '</h3>';

			if($item['show_button']){
				$icon = $item['button_icon'] ? $item['button_icon'] : 'long-arrow-right';
				$tile .= '<div class="button is-transparent is-transparent-90">' . $item['button_text'] . ' <i class="fa fa-' . $icon . '" aria-hidden="true"></i></div>';
			}

			$tile .= '</article>';
			$tile .= '</div>';
			$tile .= '</a>';
			break;
		case 'social':
			$tile = '<div class="tile is-parent is-6">';
			$tile .= '<article class="social tile is-child ' . $item['colour'] . '">';
			if($item['social_media'] == 'instagram') {
			    if($item['image']){
			        $tile .= '<img data-src="' . $item['image'] . '"/>';
			    }

			} else {
				if($item['image']){
					$tile .= '<img data-src="' . $item['image'] . '"/>';
				}
			}
			$tile .= '</article>';
			break;
	}
	$tile .= '</div>';
	return $tile;
}

function create_section_line($items, $reverse){
	if($reverse) $reverse_class = 'is-reverse';
	else $reverse_class = '';

	$line = '<div class="tile is-ancestor ' . $reverse_class . '">';
	$total_items = count($items);
	switch ($total_items) {
		case 1:
			$line .= create_section_tile($items[0]);
			break;
		case 2:
			$line .= create_section_tile($items[0]);
			$line .= create_section_tile($items[1]);
			break;
		case 3:
			$line .= '<div class="tile is-6 is-vertical">';
			$line .= create_section_tile($items[0]);
			$line .= create_section_tile($items[1]);
			$line .= '</div>';
			$line .= create_section_tile($items[2]);
			break;
		case 4:
			$line .= '<div class="tile is-6 is-vertical">';
			$line .= '<div class="tile">';
			$line .= create_section_tile($items[0]);
			$line .= create_section_tile($items[1]);
			$line .= '</div>';
			$line .= create_section_tile($items[2]);
			$line .= '</div>';
			$line .= create_section_tile($items[3]);
			break;

		default:
			# code...
			break;
	}
	if($total_items == 1) {
	} else {
		# code...
	}
	$line .= '</div>';
	return $line;
}

function the_recursive_section($items) {
	$max_elemnts_per_line = 4;
	$total_lines = ceil(count($items) / $max_elemnts_per_line);
	$section = '';
	for ($i = 0; $i < $total_lines; $i++) {
		$temp_items = array_slice($items, $i * $max_elemnts_per_line, $max_elemnts_per_line);
		$section .= create_section_line($temp_items, $i % 2 != 0);
	}
	echo $section;
}


/**
 * ACF Schema Box
 */
add_action( 'wp_head', 'rdln_acf_schema' );
function rdln_acf_schema() {
    global $post;
    $schema_content = get_field( 'post_schema_content', $post->ID );

    if ( ! empty( $schema_content ) ) {
        echo '<!-- ACF schema -->';
        echo $schema_content;
        echo '<!-- End ACF schema -->';
    }
}