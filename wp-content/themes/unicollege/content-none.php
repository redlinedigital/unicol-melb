<?php
/**
 * The template part for displaying a message that posts cannot be found
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>

<div class="content-wrap">
 	<div class="content">
		<section class="main content-page">
			<div class="container">
				<div class="columns">
					<div class="column">
						<article id="post-220" class="post-220 page type-page status-publish hentry has-text-centered">
							<header class="entry-header">
								<h1 class="entry-title museo-slab-300 blue">Nothing Found</h1>
							</header>
							<div class="entry-content">
								<?php if ( is_search() ) : ?>
									<p><?php _e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'twentysixteen' ); ?></p>
									<?php get_search_form(); ?>
								<?php else : ?>
									<p><?php _e( 'It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.', 'twentysixteen' ); ?></p>
									<?php get_search_form(); ?>
								<?php endif; ?>
							</div>
						</article>
					</div>
				</div>
			</div>
		</section>
 	</div>
</div>
