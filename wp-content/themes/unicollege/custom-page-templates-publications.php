<?php
/*
Template Name: Publications
*/
get_header(); ?>

<div class="content-wrap">
    <div class="content">
        <?php
        get_template_part( 'breadcrumb' );
        ?>

        <section class="main content-page">
            <div class="container">
                <header class="entry-header">
                    <h1 class="entry-title museo-slab-300 blue">
                        <?php
                        $current_category = single_cat_title("", false);
                        if ( $current_category != null ) echo $current_category;
                        else single_post_title();
                        ?>
                    </h1>
                </header>
                
                <?php
                if ( have_rows( 'interactive_publications' ) ) :
                    ?>
                    <ul class="publications wide">
                    <?php
                    while ( have_rows( 'interactive_publications' ) ) :
                        the_row();
                        ?>
                        <li>
                            <div style="position:relative;padding-top:max(60%,326px);height:0;width:100%">
                                <iframe sandbox="allow-top-navigation allow-top-navigation-by-user-activation allow-downloads allow-scripts allow-same-origin allow-popups allow-modals allow-popups-to-escape-sandbox" allowfullscreen="true" style="position:absolute;border:none;width:100%;height:100%;left:0;right:0;top:0;bottom:0;" src="<?= get_sub_field( 'publications_iframe_url' ); ?>"></iframe>
                            </div>
                            <?php 
                            $heading = get_sub_field( 'publications_heading' );
                            echo ( $heading ? '<h2>' . $heading . '</h2>' : '' );
                            ?>
                        </li>
                        <?php
                    endwhile;
                    ?>
                    </ul>
                <?php
                endif;

                if ( have_rows('publications') ) :
                    ?>
                    <ul class="publications">
                        <?php
                        while ( have_rows( 'publications') ) :
                            the_row();
                            $publications_image     = get_sub_field( 'publications_image' );
                            $publications_image_url = wp_get_attachment_image_src( $publications_image['ID'], 'publications' );
                            $publications_heading   = get_sub_field( 'publications_heading' );
                            $publications_document  = get_sub_field( 'publications_document' );
                            ?>
                            <li>
                                <a href="<?php echo $publications_document['url']; ?>" target="_blank">
                                    <img src="<?php echo $publications_image_url[0]; ?>" />
                                    <h2><?php echo $publications_heading; ?></h2>
                                    <p><?php echo $feature_text; ?></p>
                                </a>
                            </li>
                            <?php
                        endwhile;
                        ?>
                    </ul>
                    <?php
                endif;
                ?>

                </nav>
            </div>
        </section>
        <aside class="sidebar aside">
            <?php get_sidebar(); ?>
        </aside>
    </div>
</div>
<?php
get_footer();
