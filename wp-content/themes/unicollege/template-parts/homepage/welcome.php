<?php
$welcome_show     = get_field( 'welcome_show' );
$welcome_title    = get_field( 'welcome_title' );
$welcome_subtitle = get_field( 'welcome_subtitle' );
$welcome_text     = get_field( 'welcome_text' );
$welcome_image    = get_field( 'welcome_image' );

if ( $welcome_show == true ) :
    ?>
    <section class="home-welcome content-page">
        <div class="container">
            <div class="columns">
                <div class="column is-12">
                <?php if ($welcome_title) : ?>
                    <h1>
                        <?= $welcome_title ?>
                        <?php
                        if ( $welcome_subtitle ) :
                            ?>
                            <span class="style-1">
                                <?= $welcome_subtitle ?>
                            </span>
                            <?php
                        endif;
                        ?>
                    </h1>
                <?php endif; ?>
                </div>
            </div>
            <div class="columns">
                <div class="column is-8">
                    <?php if ($welcome_text) : ?>
                        <?php echo apply_filters( 'the_content', $welcome_text );  ?>
                    <?php endif; ?>
                </div>
            </div>
            <?php if ($welcome_image) : ?>
                <img class="container-img" src="<?= $welcome_image ?>" alt="img">
            <?php endif; ?>
        </div>
    </section>
    <?php
endif;