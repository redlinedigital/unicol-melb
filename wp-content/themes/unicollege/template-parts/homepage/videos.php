<?php
$show_videos           = get_field( 'home_videos_show' );
$videos_heading        = get_field( 'home_videos_heading' );
$videos_intro          = get_field( 'home_videos_intro' );
$videos_footer_heading = get_field( 'home_videos_footer_heading' );


if ( $show_videos && have_rows('home_videos_videos') ) :
    ?>
    <section class="home-videos">
        <div class="content-page">
            <div class="container">
                <div class="columns">
                    <div class="column is-12">
                        <?php echo ( $videos_heading ? '<h2>' . esc_attr( $videos_heading ) . '</h2>' : '' ); ?>
                        <?php echo ( $videos_intro ? $videos_intro : '' ); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="home-videos__videos">
            <div class="container wide">
                <div class="home-videos__grid">
                    <?php
                    while( have_rows('home_videos_videos') ) :
                        the_row();
                        $video_id = get_sub_field( 'youtube_id' );
                        $image    = get_sub_field( 'image' );
                        ?>
                        <a href="https://www.youtube.com/watch?v=<?php echo esc_attr( $video_id ); ?>" class="popup-iframe">
                            <img src="<?php echo esc_attr( $image['sizes']['large'] ); ?>" alt="<?php echo esc_attr( $image['alt'] ); ?>">
                        </a>
                        <?php
                    endwhile;
                    ?>
                </div>
                <?php echo ( $videos_footer_heading ? '<h3>' . esc_attr( $videos_footer_heading ) . '</h3>' : '' ); ?>
            </div>
        </div>
    </section>
    <?php
endif;