<?php
if( have_rows( 'home_hero' ) ) :
    $slide_count = count( get_field('home_hero') );
    ?>
    <section class="homepage-hero slick-slider <?= ( $slide_count === 1 ? 'slick-hide-dots' : '' ); ?>">
        <?php
        while( have_rows('home_hero') ):
            the_row();
        
            $heading        = get_sub_field('home_hero_text1');
            $subheading     = get_sub_field('home_hero_text2');
            $page_link      = get_sub_field('home_hero_page');
            $link_text      = get_sub_field('home_hero_link_text');
            $image          = get_sub_field('home_hero_image');
            $video          = get_sub_field('home_hero_video');
            $video_image    = get_sub_field('home_hero_video_image');

            // Generate the image (once) ensuring the original isn't being used.
            // This ensures it is properly optimised.
            // aq_resize( $url, $width, $height, $crop, $single, $upscale )
            if ( $image ) {
                list($img_src)        = aq_resize( $image['url'], 2000, 900, true, false, true );
                list($img_src_tablet) = aq_resize( $image['url'], 810, 720, true, false, true );
                list($img_src_mobile) = aq_resize( $image['url'], 375, 520, true, false, true );
            }
            ?>
            <div>
                <div class="homepage-hero__slide <?= $hero_colour_bg ?>">

                    <?php
                    if ( ! $video && $img_src ) {
                        ?>
                        <img srcset="<?= $img_src_mobile ?> 375w,
                                    <?= $img_src_tablet ?> 768w,
                                    <?= $img_src ?> 1345w"
                            sizes="(max-width: 375px) 375px,
                                    (max-width: 768px) 768px,
                                    769px"
                            src="<?=$img_src?>"
                            alt="<?= $image['alt'] ?>"
                            class="homepage-hero-slide__img" />
                        <?php
                    }
                    ?>

                    <div class="container">
                        <div class="slide-content">
                            <?php
                            echo ( $heading ? '<h2 class="homepage-hero-slide__heading">' . $heading . '</h2>' : '' );
                            echo ( $subheading ? '<h3 class="homepage-hero-slide__subheading">' . $subheading . '</h3>' : '' );

                            if ( $link_text && $page_link ) {
                                echo '<a href="' . $page_link . '" class="homepage-hero-slide__cta">' . $link_text . '</a>';
                            }
                            ?>
                        </div>
                    </div>

                    <?php
                    if ( $video ) {
                        ?>
                        <video
                            playsinline
                            autoplay
                            muted
                            loop
                            disablePictureInPicture
                            poster="<?= $video_image ?>"
                            >
                                <source src="<?= $video ?>" type="video/mp4">
                        </video>
                        <?php
                    }
                    ?>
                </div>
                </div>
            <?php
        endwhile;
        ?>
    </section>
    <?php
endif;