<?php
if( have_rows('home_sections') ):
    while( have_rows('home_sections') ): the_row();
        ?>
        <section class="sections">
            <div class="container">
                <?php
                /* <div class="museo-sans-700 section-title"><?php the_sub_field('home_section_title'); ?></div> */

                $items = array();
                while( have_rows('home_section_content') ){
                    the_row();
                    $item = array(
                        'row_layout'   => get_row_layout(),
                        'page'         => get_sub_field('page'),
                        'image'        => get_sub_field('image'),
                        'description'  => get_sub_field('description'),
                        'colour'       => get_sub_field('colour'),
                        'social_media' => get_sub_field('social_media'),
                        'show_button'  => get_sub_field('show_button'),
                        'button_text'  => get_sub_field('button_text'),
                        'button_icon'  => get_sub_field('button_icon')
                    );
                    array_push($items, $item);
                }

                the_recursive_section($items);
                ?>
        </section>
        <?php
	endwhile;
endif;