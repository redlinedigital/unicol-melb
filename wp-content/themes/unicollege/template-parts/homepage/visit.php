<?php
$visit_show     = get_field( 'visit_show' );
$visit_title    = get_field( 'visit_title' );
$visit_subtitle = get_field( 'visit_subtitle' );
$visit_text     = get_field( 'visit_text' );
$visit_image    = get_field( 'visit_image' );

if ( $visit_show == true ) :
    ?>
    <section class="home-visit content-page">
        <div class="container">
        <div class="columns">
            <div class="column is-12">
                <?php if ($visit_title) : ?>
                    <h2 class="h1"><?= $visit_title ?></h2>
                <?php endif; ?>
                <?php if ($visit_subtitle) : ?>
                    <p class="style-1"><?= $visit_subtitle ?></p>
                <?php endif; ?>
            </div>
        </div>
        <div class="columns">
            <div class="column is-8">
                <?php if ($visit_text) : ?>
                    <?php echo apply_filters( 'the_content', $visit_text );  ?>
                <?php endif; ?>
            </div>
        </div>
        <?php if ($visit_image) : ?>
            <img class="container-img" src="<?= $visit_image ?>" alt="img">
        <?php endif; ?>
    </div>
    </section>
    <?php
endif;