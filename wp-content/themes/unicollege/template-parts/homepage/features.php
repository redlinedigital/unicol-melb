<?php
if( have_rows('home_features') ) {
    $w = 0;
    ?>
    <section class="featured-items featured-items-new">
        <div class="container">
            <div class="columns">
                <div class="column is-7 blue-bg left-side">
                    <?php
                    // loop through the rows of data
                    $i = 1;
                    while ( have_rows('home_features') ) : the_row();
                        if ($i == 1 && get_row_layout() == 'page') {
                            $home_featured_page_title = get_sub_field('title');
                            $home_featured_page_article = get_sub_field('page');
                            $home_featured_page_button_text = get_sub_field('button_text');
                            $home_featured_page_image = get_sub_field('image');
                            // $home_featured_page_see_all = get_sub_field('home_featured_news_a_see_all');
                            $home_featured_page_colour = get_sub_field('colour');
                            ?>
                            <div class="featured-item first-item ">
                                <div class="featured-image">
                                    <img src="<?= $home_featured_page_image['url'] ?>" alt="<?= $home_featured_page_title ?>>">
                                </div>
                                <div class="featured-title <?= $home_featured_page_colour ?>">
                                    <a href="<?= $home_featured_page_article ?>" class="button is-transparent is-transparent-90">
                                        <?= $home_featured_page_button_text ?> <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>
                            <?php
                        }
                        $i++;
                    endwhile;
                    ?>
                </div>
                <div class="column is-5 right-side">
                    <?php
                    // loop through the rows of data
                    $i = 1;
                    while ( have_rows('home_features') ) : the_row();
                        if ($i == 2 && get_row_layout() == 'video') {
                            $home_featured_video_title  = get_sub_field('title');
                            $home_featured_video_image  = get_sub_field('image');
                            $home_featured_video_colour = get_sub_field('colour');
                            $home_featured_video_text   = get_sub_field('text');
                            $home_featured_video_id     = get_sub_field( 'youtube_video_id' );
                            ?>
                            <div class="featured-item <?= $home_featured_video_colour ?>">
                                <div class="featured-title">
                                    <div class="featured-title-insert">
                                        <h3 class="title"><?= $home_featured_video_title ?></h3>
                                        <p><?=  $home_featured_video_text ?></p>
                                        <a href="https://www.youtube.com/watch?v=<?php echo esc_attr( $home_featured_video_id ); ?>" class="button is-transparent is-transparent-90 popup-iframe">
                                            Play video <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="featured-image">
                                    <img src="<?= $home_featured_video_image['url'] ?>" alt="<?= $home_featured_video_title ?>">
                                </div>
                            </div>
                            <?php
                        }
                        $i++;
                    endwhile;
                    
                    // loop through the rows of data
                    $i = 1;
                    while ( have_rows('home_features') ) : the_row();
                        if ($i == 3 && get_row_layout() == 'page') {
                            $home_featured_page_title = get_sub_field('title');
                            $home_featured_page_article = get_sub_field('page');
                            $home_featured_page_button_text = get_sub_field('button_text');
                            $home_featured_page_image = get_sub_field('image');
                            // $home_featured_page_see_all = get_sub_field('home_featured_news_a_see_all');
                            $home_featured_page_colour = get_sub_field('colour');
                            $home_featured_page_text = get_sub_field('text');
                            ?>
                            <div class="featured-item <?= $home_featured_page_colour ?>">
                                <div class="featured-image">
                                    <img src="<?= $home_featured_page_image['url'] ?>" alt="<?= $home_featured_page_title ?>">
                                </div>
                                <div class="featured-title">
                                    <div class="featured-title-insert">
                                        <h3 class="title"><?= $home_featured_page_title ?></h3>
                                        <p><?= $home_featured_page_text ?></p>
                                        <a href="<?= $home_featured_page_article ?>" class="button is-transparent is-transparent-90">
                                            <?= $home_featured_page_button_text ?> <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                        $i++;
                    endwhile;
                    ?>
                </div>
            </div>
        </div>
    </section>
    <?php
}
