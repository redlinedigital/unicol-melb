<?php
$args = array(
    'post_type'           => 'post',
    'posts_per_page'      => 3,
    'ignore_sticky_posts' => true,
);

$the_query = new WP_Query( $args );
 
if ( $the_query->have_posts() ) {
    ?>
    <section class="latest-posts">
        <div class="container content">
            <h2>
                <a href="<?= home_url( 'category/blog' ); ?>">University College News &amp; Blog</a>
            </h2>
            <div class="latest-posts__results">
                <?php
                while ( $the_query->have_posts() ) {
                    $the_query->the_post();
                    get_template_part( 'intro-post-stacked' );
                }
                ?>
            </div>
            <div class="latest-posts__all">
                <a href="<?= home_url( 'category/blog' ); ?>" class="button btn-dark-red">All articles</a>
            </div>
        </div>
    </section>
    <?php
}

wp_reset_postdata();