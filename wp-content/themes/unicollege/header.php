<!doctype html>
<html class="no-js" lang="">
<head>
    <meta charset="utf-8">
    <script>
    (function(d) {
        var config = {kitId: 'ioy8ciz', scriptTimeout: 3000, async: true },
        h=d.documentElement,t=setTimeout(function(){h.className=h.className.replace(/\bwf-loading\b/g,"")+" wf-inactive";},config.scriptTimeout),tk=d.createElement("script"),f=false,s=d.getElementsByTagName("script")[0],a;h.className+=" wf-loading";tk.src='https://use.typekit.net/'+config.kitId+'.js';tk.async=true;tk.onload=tk.onreadystatechange=function(){a=this.readyState;if(f||a&&a!="complete"&&a!="loaded")return;f=true;clearTimeout(t);try{Typekit.load(config)}catch(e){}};s.parentNode.insertBefore(tk,s)
    })(document);
    </script>
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo IMG; ?>/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo IMG; ?>/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo IMG; ?>/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo IMG; ?>/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo IMG; ?>/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo IMG; ?>/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo IMG; ?>/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo IMG; ?>/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo IMG; ?>/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo IMG; ?>/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo IMG; ?>/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo IMG; ?>/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo IMG; ?>/favicon-16x16.png">
    <link rel="manifest" href="<?php echo IMG; ?>/manifest.json">
    <meta name="google-site-verification" content="2LEorQRARobECcoEfTw7ibCYHH7sWMJim7vMD26Ujio" />
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?php echo IMG; ?>/ms-icon-144x144.png">
    <meta name="theme-color" content="#5692ce" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="preconnect" href="https://maxcdn.bootstrapcdn.com">
    <link rel="preconnect" href="https://cdn.linearicons.com">
    <link rel="preconnect" href="https://use.typekit.net">
    <link rel="preconnect" href="https://fonts.typekit.net">
    <link rel="preconnect" href="https://p.typekit.net">
    <link rel="preconnect" href="https://scontent.cdninstagram.com">
    <link rel="preconnect" href="https://s.tribalfusion.com">
    <link rel="preconnect" href="https://a.tribalfusion.com">
    <link rel="preconnect" href="https://stats.g.doubleclick.net">
    <link rel="preconnect" href="https://googleads.g.doubleclick.net">
    <link rel="preconnect" href="https://cm.g.doubleclick.net">
    <link rel="preconnect" href="https://ssl.google-analytics.com">
    <link rel="preconnect" href="https://www.youtube.com">
    <link rel="preconnect" href="https://static.addtoany.com">
    <link rel="preconnect" href="https://s.ytimg.com">
    <link rel="preconnect" href="https://i.ytimg.com">
    <link rel="preconnect" href="https://s.w.org">
    <link rel="preconnect" href="https://www.google.com">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link rel="stylesheet" type="text/css" href="<?php bloginfo( 'stylesheet_url' ); ?>?v3" />
    <link rel="stylesheet" href="https://cdn.linearicons.com/free/1.0.0/icon-font.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?=CSS?>/lightbox.min.css">

    <meta name="facebook-domain-verification" content="sxzt59q7n4rrkr06szedfohwvvrh2l" />

    <!-- Facebook Pixel Code -->
    <script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};
    if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
    n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];
    s.parentNode.insertBefore(t,s)}(window,document,'script',
    'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '324549143080951'); 
    fbq('track', 'PageView');
    </script>
    <noscript>
    <img height="1" width="1" 
    src="https://www.facebook.com/tr?id=324549143080951&ev=PageView
    &noscript=1"/>
    </noscript>
    <!-- End Facebook Pixel Code -->

  <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
  <header class="header">
	  <section class="section-header is-medium">
        <div class="container">
           <nav class="nav">
             <div class="nav-left">
               <a class="nav-item logo" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="Go to home page">
                 <?php bloginfo( 'name' ) ?>
               </a>
             </div>

             <span id="nav-toggle" class="nav-toggle no-print">
               <span></span>
               <span></span>
               <span></span>
             </span>


             <!-- QUICK MENU DESKTOP -->
            <div class="nav-right nav-menu desktop no-print">
              <?php the_quick_menu_desktop(); ?>
              <?php //wp_nav_menu( array( 'theme_location' => 'quick_menu', 'menu_class' => 'nav-menu' ) ); ?>

              <div class="nav-item social-media-menu">
                <?php the_social_media_mobile(); ?>
              </div>
              <?php if(get_field('show_support_flag', 'option')): ?>
              <div class="nav-item">
                  <!--<a href="" class="donation">
                    <div class="heart"></div>
                  </a>-->
                  <a class="button btn-dark-red" href="<?php the_field('donate_page', 'option'); ?>">Donate Now <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
                  <a class="button btn-dark-blue" href="<?php the_field('support_page', 'option'); ?>">APPLY NOW <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
              </div>
              <?php endif; ?>
           </div>


           <!-- PRIMARY MENU MOBILE -->
            <div class="is-hidden-tablet no-print">
              <div id="nav-menu" class="nav-menu">
                <?php

                $menu_name = 'primary_menu';
                if ( ( $locations = get_nav_menu_locations() ) && isset( $locations[ $menu_name ] ) ) {
                  $menu = wp_get_nav_menu_object( $locations[ $menu_name ] );
                  $menu_items = wp_get_nav_menu_items($menu->term_id);
                  $count = 0;
                  $submenu = false;
                  $menu_list = '';

                   echo print_primary_menu_mobile($menu_items, "0");
                 }
                ?>
                <?php if(get_field('show_support_flag', 'option')): ?>
                <!--<div class="nav-item">
                  <a href="" class="donation is-unselectable">
                    <div class="columns is-mobile">
                      <div class="column is-half is-offset-one-quarter">
                        <div class="heart"></div> Support UC
                      </div>
                    </div>
                  </a>
                </div>-->
                <div class="nav-item-btn-wrapper">
                    <a class="button btn-dark-red" href="<?php the_field('donate_page', 'option'); ?>">Donate Now <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
                    <a class="button btn-dark-blue" href="<?php the_field('support_page', 'option'); ?>">APPLY NOW <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
                </div>
                <?php endif; ?>
                <div class="social-media-menu">
                  <?php the_social_media_mobile(); ?>
                </div>
              </div>
            </div>

           </nav>
        </div>
    </section>
    <section class="section-menu no-print">
      <div class="container">
        <div class="is-hidden-mobile">
            <?php
            echo clean_custom_menu('primary_menu');
            ?>
        </div>

         <div class="is-hidden-tablet mobile-quick-menu">
           <div class="columns is-mobile">
             <?php the_quick_menu_mobile(); ?>
           </div>
         </div>
      </div>
    </section>
  </header>
