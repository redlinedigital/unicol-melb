
<div class="container">
  <div class="columns">
  <div class="column is-12">


  <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <?php
    $pagetype = get_post_type();
    ?>

    <?php
    // STUDENTS PAGE

    if( $pagetype !== 'event' ): ?>
      <header class="entry-header">
        <h1 class="entry-title museo-slab-300 blue"><?php the_title(); ?></h1>
      </header>
    <?php endif; ?>

    <div class="entry-content">
      <?php the_content(); ?>


      


    </div><!-- .entry-content -->

    <!-- <footer class="entry-meta"> -->
      <?php //edit_post_link( __( 'Edit Page'), '<span class="edit-link">', '</span>' ); ?>
    <!-- </footer> -->
  </article><!-- #post-<?php the_ID(); ?> -->
</div>
</div>
</div>
