<?php
/**
 * The Template for displaying a post
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
?>
<article id="post-<?php the_ID(); ?>" class="intro-post">
	<div class="columns">
		<div class="column is-hidden-mobile is-3">
			<figure class="image is-square">
				<?php
				if(has_post_thumbnail()) {
				    the_post_thumbnail( array(269, 269) );
				} else {
				    // echo '<img src="https://unsplash.it/500/?random" />';
				}
				?>
			</figure>
		</div>
		<div class="column">
			<hr class="small"/>
			<a href="<?php echo get_permalink(); ?>">
				<h2 class="museo-slab-300 blue-darker h3"><?php the_title(); ?></h2>
			</a>
			<p>
				<?php the_excerpt(); ?>
			</p>
			<a href="<?php echo get_permalink(); ?>" class="button is-info">
				READ ARTICLE <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
			</a>
		</div>
	</div>
</article>
