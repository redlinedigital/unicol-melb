<?php
/**
 * The Template for displaying all single posts
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

<div class="content-wrap">
	<div class="content">
		<?php
		get_template_part( 'breadcrumb' );
		?>
		<section class="main content-page">
			<?php

				while ( have_posts() ) : the_post();
					get_template_part( 'content', 'post' );
				endwhile; // end of the loop.
			?>
		</section>
		<aside class="sidebar aside">
			<?php get_sidebar(); ?>
		</aside>
	</div><!-- End .content -->
</div><!-- End .content-wrap -->
<?php get_footer(); ?>
