<?php
/**
 * Hubink functions and definitions
 *
 * Set up the donation plugin.
 *
 * @package WordPress
 * @subpackage Hubink
 */

# add other payment gateway
require 'securepay_standard_gateway.php';


/**
 * Custom Form Fields
 *
 * @param $form_id
 */
function give_myprefix_custom_form_fields( $form_id ) {
	if( have_rows('donation_directions', 'option') ): ?>
	<h3 class="museo-slab-700 blue">Where would you like to direct your donation</h3>
		<?php
		while( have_rows('donation_directions', 'option') ): the_row();
		?>
		<div class="<?php echo str_replace(' ', '_', strtolower(get_sub_field('group_title'))); ?>">
		<strong><?php echo get_sub_field('group_title'); ?></strong>
		<div class="columns form-radio">
			<?php
			while( have_rows('directions', 'option') ): the_row();
			?>
			<div class="column">
				<div class="radio">
		        <label>
		          <input type="radio" name="give_direction" value="<?php the_sub_field('direction'); ?>"><i class="helper"></i> <?php the_sub_field('direction'); ?>
		        </label>
				</div>
			</div>
			<?php endwhile; ?>
		</div>
		<?php endwhile; ?>
	</div>
	<?php endif; ?>

	<strong class="syme_chair">Syme Dining Hall Chair Fund</strong>
	<div class="columns form-radio syme_chair">
		<div class="column">
			<div class="radio">
        <label>
          <input type="radio" name="give_hair_chair" value="Dining Hall Chair - $500 donation" class="has-value"><i class="helper"></i> Dining Hall Chair - $500 donation
				</label>
			</div>
		</div>
		<div class="column">
			<div class="radio">
        <label>
          <input type="radio" name="give_hair_chair" value="High Table Chair - $1000 donation" class="has-value"><i class="helper"></i> High Table Chair - $1000 donation
				</label>
			</div>
		</div>
	</div>

	<div class="columns inscrition_chair">
		<div class="column">
			<p id="give-first-name-wrap" class="form-row form-row-first form-group">
				<input class="give-input required has-value" type="text" name="give_inscrition_chair" id="give-inscrition-chair">
				<label class="give-label control-label" for="give-inscrition-chair">
					What would you like inscribed on the chair?
				</label>
				<i class="bar"></i>
			</p>
		</div>
	</div>

		<strong class="remain_anonymous">Would you like donation to remain anonymous?</strong>
		<div class="columns remain_anonymous">
			<div class="column">
				<div class="checkbox">
		      <label>
		        <input type="checkbox" name="give_remain_anonymous" value="Yes"><i class="helper"></i> Yes, I'd like to remain anonymous!
		      </label>
		    </div>
			</div>
		</div>
	<?php
}
add_action( 'give_after_donation_levels', 'give_myprefix_custom_form_fields', 10, 1 );


/**
 * Custom Form Fields
 *
 * @param $form_id
 */
function give_myprefix_custom_form_fields_user_info( $form_id ) {
		?>
		<div class="columns">
			<div class="column">
				<div class="form-group">
		    	<input type="text" name="give_phone" value="" aria-required="true" aria-invalid="false">
					<label for="give_phone" class="control-label">Phone *</label>
		    </div>
			</div>
		</div>
		<div class="columns">
			<div class="column">
				<div class="form-group">
		    	<input type="text" name="give_yearsatuc" value="" aria-required="true" aria-invalid="false">
					<label for="give_yearsatuc" class="control-label">Years spent at UC - if applicable</label>
		    </div>
			</div>
		</div>
	<?php
}
add_action( 'give_purchase_form_user_info', 'give_myprefix_custom_form_fields_user_info', 10, 1 );


/**
 * Validate Custom Field
 *
 * @description check for errors without custom fields
 *
 * @param $valid_data
 * @param $data
 */
function give_myprefix_validate_custom_fields( $valid_data, $data ) {
	//Check for a referral data
	if ( empty( $data['give_phone'] ) ) {
		give_set_error( 'give_phone', __( 'Please tell us your phone number.', 'give' ) );
	}
}
add_action( 'give_checkout_error_checks', 'give_myprefix_validate_custom_fields', 10, 2 );


/**
 * Add Field to Payment Meta
 *
 * @description store the custom field data in the payment meta
 *
 * @param $payment_meta
 *
 * @return mixed
 */
function give_myprefix_store_custom_fields( $payment_meta ) {
	$payment_meta['direction'] = isset( $_POST['give_direction'] ) ? wp_strip_all_tags( $_POST['give_direction'] ) : '--';
	$payment_meta['remain_anonymous'] = isset( $_POST['give_remain_anonymous'] ) ? wp_strip_all_tags( $_POST['give_remain_anonymous'] ) : '';
	$payment_meta['hair_chair'] = isset( $_POST['give_hair_chair'] ) ? wp_strip_all_tags( $_POST['give_hair_chair'] ) : '';
	$payment_meta['inscrition_chair'] = isset( $_POST['give_inscrition_chair'] ) ? wp_strip_all_tags( $_POST['give_inscrition_chair'] ) : '';
	$payment_meta['yearsatuc'] = isset( $_POST['give_yearsatuc'] ) ? wp_strip_all_tags( $_POST['give_yearsatuc'] ) : '';
	$payment_meta['phone'] = isset( $_POST['give_phone'] ) ? wp_strip_all_tags( $_POST['give_phone'] ) : '';

	return $payment_meta;
}

add_filter( 'give_payment_meta', 'give_myprefix_store_custom_fields' );


/**
 * Show Data in Transaction Details
 *
 * @description show the custom field(s) on the transaction page
 *
 * @param $payment_meta
 * @param $user_info
 */
function give_myprefix_purchase_details( $payment_meta, $user_info ) {
	//Bounce out if no data for this transaction
	if ( isset( $payment_meta['phone'] ) ) {
	?>
	<br/>
	<div class="phone-data">
		<strong><?php echo __( 'Phone:', 'give' ); ?></strong>
		<span style="color:red">
		<?php echo $payment_meta['phone']; ?>
		</span>
	</div>
	<?php
	}

	//Bounce out if no data for this transaction
	if ( isset( $payment_meta['yearsatuc'] ) ) {
	?>
	<br/>
	<div class="yearsatuc-data">
		<strong><?php echo __( 'Years spent at UC:', 'give' ); ?></strong>
		<span style="color:red">
		<?php echo $payment_meta['yearsatuc']; ?>
		</span>
	</div>
	<?php
	}
}

add_action( 'give_payment_personal_details_list', 'give_myprefix_purchase_details', 10, 2 );


/**
 * Show Data in Transaction Details
 *
 * @description show the custom field(s) on the transaction page
 *
 * @param $payment_meta
 * @param $user_info
 */
function give_custom__th_after( $payment_id ) {
	?>
	<?php
}
add_action( 'give_donation_details_thead_after', 'give_custom__th_after', 10, 2 );


/**
 * Show Data in Transaction Details
 *
 * @description show the custom field(s) on the transaction page
 *
 * @param $payment_id
 */
function give_custom__td_after( $payment_id ) {
	$payment_meta   = give_get_payment_meta( $payment_id );
 ?>
	<strong><?php _e( 'Remain anonymous', 'give' ) ?>: </strong>
	<span style="color: red">
	<?php if($payment_meta['remain_anonymous'] != "") echo 'YES'; else echo 'NO'; ?>
	</span>

	<br/>
	<br/>

	<strong><?php _e( 'Donation directed to', 'give' ) ?>: </strong>
	<span style="color: red">
	<?php echo $payment_meta['direction']; ?>
	</span>

 	<br/>
 	<br/>

	<strong><?php _e( 'Chair', 'give' ) ?>: </strong>
	<span style="color: red">
	<?php echo $payment_meta['hair_chair']; ?>
	</span>

 	<br/>
 	<br/>

	<strong><?php _e( 'Inscription on the chair', 'give' ) ?>: </strong>
	<span style="color: red">
	<?php echo $payment_meta['inscrition_chair']; ?>
	</span>
 <?php
}
add_action( 'give_donation_details_tbody_after', 'give_custom__td_after', 10, 2 );


/**
 * Show Data in Transaction Details
 *
 * @description show the custom field(s) on the transaction page
 *
 * @param $payment_meta
 * @param $user_info
 */
function new_personal_info_text() {
	return __('Contact Details', 'give');
}
add_filter('give_checkout_personal_info_text', 'new_personal_info_text');



/**
 * Email template tag: direction
 * Destination of the donation
 *
 * @param int $payment_id
 *
 * @return string $direction
 */
function give_direction_tag_donation( $payment_id ) {
	$payment_meta = give_get_payment_meta( $payment_id );
	return $payment_meta['direction'];
}


/**
 * Email template tag: inscrition_chair
 * Inscrition on the chair
 *
 * @param int $payment_id
 *
 * @return string $inscrition_chair
 */
function give_inscrition_chair_tag_donation( $payment_id ) {
	$payment_meta = give_get_payment_meta( $payment_id );
	return $payment_meta['inscrition_chair'];
}

/**
 * Email template tag: remain_anonymous
 * If the user chose stay anonymous
 *
 * @param int $payment_id
 *
 * @return string $remain_anonymous
 */
function give_remain_anonymous_tag_donation( $payment_id ) {
	$payment_meta = give_get_payment_meta( $payment_id );
	return $payment_meta['remain_anonymous'];
}

/**
 * Email template tag: hair_chair
 * The chair option
 *
 * @param int $payment_id
 *
 * @return string hair_chair
 */
function give_hair_chair_tag_donation( $payment_id ) {
	$payment_meta = give_get_payment_meta( $payment_id );
	return $payment_meta['hair_chair'];
}




/**
 * Add custom tags in the EMAIL template
 */
if( function_exists('give_add_email_tag') ) {
	// Setup default tags array
	$email_tags = array(
		array(
			'tag'         => 'direction',
			'description' => __( 'The name of destination where the donation was chosen if applicable.', 'give' ),
			'function'    => 'give_direction_tag_donation'
		),
		array(
			'tag'         => 'anonymous',
			'description' => __( 'Whether the user chose stay anonymous or not.', 'give' ),
			'function'    => 'give_remain_anonymous_tag_donation'
		),
		array(
			'tag'         => 'hair_chair',
			'description' => __( 'The chair chosen.', 'give' ),
			'function'    => 'give_hair_chair_tag_donation'
		),
		array(
			'tag'         => 'inscrition',
			'description' => __( 'The inscription the on the chair.', 'give' ),
			'function'    => 'give_inscrition_chair_tag_donation'
		)
	);

	// Add email tags
	foreach ( $email_tags as $email_tag ) {
		give_add_email_tag( $email_tag['tag'], $email_tag['description'], $email_tag['function'] );
	}
}



/**
 * Give Form Require Fields
 *
 * @description: Cusomize the prefix of this function before using. The $required_fields array key is the name of the field you would like to make required; ie " name='card_address_2' " - find using Debug Tools, etc.
 *
 * @param $required_fields
 *
 * @return mixed
 */
function xmycustomprefix_give_form_required_fields( $required_fields ) {

	//Last Name
    $required_fields['give_last'] = array(
        'error_id' => 'invalid_last_name',
        'error_message' => __( 'Please enter your last name.', 'give' )
    );

    return $required_fields;
}
add_filter( 'give_purchase_form_required_fields', 'xmycustomprefix_give_form_required_fields' );




/**
 * Give Form Custom fields on report
 *
 * @description: Cusomize the report to add the custom fields
 *
 * @param $data
 *
 * @return mixed
 */
function my_give_export_get_data_payments( $data ) {
		for ($i = 0; $i < count($data); $i++) {
			$data[$i]["direction"] = give_get_payment_meta( $data[$i]["id"] )["direction"];
		}
		return $data;
}
add_filter( 'give_export_get_data_payments', 'my_give_export_get_data_payments' );

function my_give_export_csv_cols_payments($data){
	$data["direction"] = "Donation To";
	return $data;
}
add_filter( 'give_export_csv_cols_payments', 'my_give_export_csv_cols_payments' );


function my_give_payment_receipt_after($payment){
	?>
	<tr>
		<td><strong><?php _e( 'Donation To', 'give' ); ?>:</strong></td>
		<td><?php echo give_get_payment_meta( $payment->ID )["direction"]; ?></td>
	</tr>
	<?
}
add_filter( 'give_payment_receipt_after', 'my_give_payment_receipt_after' );

?>
