<?php
/**
 * Hubink functions and definitions
 *
 * Set up the shortcodes used by the theme.
 *
 * @package WordPress
 * @subpackage Hubink
 */



// SITE URL for links
function home_shortcode() {
	return SITE;
}
add_shortcode( 'home', 'home_shortcode' );

// Wordpress URL for links
function image_shortcode() {
	return WP;
}
add_shortcode('image', 'image_shortcode');

// Wordpress URL for Uploads
function my_uploads_folder () {
	$upload_dir = wp_upload_dir();
	return $upload_dir['baseurl'];
}
add_shortcode('uploads', 'my_uploads_folder');


// Responsive videos
function my_responsive_videos ($atts, $content) {
	$atts = shortcode_atts( array(
		'columns' => 'is-12',
		'align' => 'alignleft'
	), $atts, 'youtube' );

	$q = "<div class='youtube-shortcut is-". $atts['columns'] . " align" . $atts['align'] . "'>";
	$q .= "<div class='embed-container'>$content</div>";
	$q .= "</div>";
	return $q;
}
add_shortcode('youtube', 'my_responsive_videos');


?>