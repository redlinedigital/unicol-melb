<?php
/**
 * Hubink functions and definitions
 *
 * Set up the donation plugin.
 *
 * @package WordPress
 * @subpackage Hubink
 */

# add other payment gateway
require 'securepay_standard_gateway.php';


/**
 * Custom Form Fields
 *
 * @param $form_id
 */
function give_myprefix_custom_form_fields( $form_id ) {
  if( have_rows('donation_directions', 'option') ): ?>
    <h2 class="museo-slab-700 blue h3">Where would you like to direct your donation</h2>
    <?php
    while( have_rows('donation_directions', 'option') ): the_row();
        ?>
        <div class="form-group <?php echo str_replace(' ', '_', strtolower(get_sub_field('group_title'))); ?>">
            <strong><?php echo get_sub_field('group_title'); ?></strong>
            <div class="columns form-radio">
                <?php
                while( have_rows('directions', 'option') ): the_row();
                    ?>
                    <div class="column">
                        <div class="radio">
                            <label>
                            <input type="radio" name="give_direction" value="<?php the_sub_field('direction'); ?>"><i class="helper"></i> <?php the_sub_field('direction'); ?>
                            </label>
                        </div>
                    </div>
                    <?php
                endwhile;
                ?>
            </div>
        </div>
        <?php
    endwhile;
    ?>
    <div class="form-group columns">
        <div class="column">
            <p><strong>Please specify if you’d like your donation to go towards a specific scholarship fund, or student type (i.e. rural, study discipline, international etc). If&nbsp;not&nbsp;directed, your donation will go towards our general scholarship appeal.</strong></p>
            <textarea name="give_specific_direction" id="" rows="2"></textarea>
            <i class="bar"></i>
        </div>
    </div>
    <?php
  endif;
  ?>

    <div class="form-group">
        <strong class="remain_anonymous">Would you like donation to remain anonymous?</strong>
        <div class="columns remain_anonymous">
        <div class="column">
            <div class="checkbox">
            <label>
                <input type="checkbox" name="give_remain_anonymous" value="Yes"><i class="helper"></i> Yes, I'd like to remain anonymous
            </label>
            </div>
        </div>
        </div>
    </div>
  <?php
}
add_action( 'give_after_donation_levels', 'give_myprefix_custom_form_fields', 10, 1 );


/**
 * Custom Form Fields
 *
 * @param $form_id
 */
function give_myprefix_custom_form_fields_user_info( $form_id ) {
    ?>
    <p class="form-row">
        <label for="give_phone" class="give-label">Phone *</label>
        <input type="text" class="give-input" name="give_phone" value="" aria-required="true" aria-invalid="false" placeholder="Phone">
    </p>
    <p class="form-row">
        <label for="give_yearsatuc" class="give-label">Relationship to UC (if any)</label>
        <input type="text" class="give-input" name="give_yearsatuc" value="" aria-required="true" aria-invalid="false" placeholder="Relationship to UC">
    </p>
  <?php
}
add_action( 'give_donation_form_after_email', 'give_myprefix_custom_form_fields_user_info', 10, 1 );


/**
 * Validate Custom Field
 *
 * @description check for errors without custom fields
 *
 * @param $valid_data
 * @param $data
 */
function give_myprefix_validate_custom_fields( $valid_data, $data ) {
  //Check for a referral data
  if ( empty( $data['give_phone'] ) ) {
    give_set_error( 'give_phone', __( 'Please tell us your phone number.', 'give' ) );
  }
}
add_action( 'give_checkout_error_checks', 'give_myprefix_validate_custom_fields', 10, 2 );


/**
 * Add Field to Payment Meta
 *
 * Store the custom field data  meta.
 *
 * @param $payment_id
 * @param $payment_data
 *
 * @return mixed
 */
function myprefix123_give_donations_save_custom_fields( $payment_id, $payment_data ) {
	if ( isset( $_POST['give_direction'] ) ) {
		give_update_meta( $payment_id, 'give_direction', wp_strip_all_tags( $_POST['give_direction'] ) );
	}
	if ( isset( $_POST['give_specific_direction'] ) ) {
		give_update_meta( $payment_id, 'give_specific_direction', wp_strip_all_tags( $_POST['give_specific_direction'] ) );
	}
	if ( isset( $_POST['give_remain_anonymous'] ) ) {
		give_update_meta( $payment_id, 'give_remain_anonymous', wp_strip_all_tags( $_POST['give_remain_anonymous'] ) );
	}
	if ( isset( $_POST['give_yearsatuc'] ) ) {
		give_update_meta( $payment_id, 'give_yearsatuc', wp_strip_all_tags( $_POST['give_yearsatuc'] ) );
	}
	if ( isset( $_POST['give_phone'] ) ) {
		give_update_meta( $payment_id, 'give_phone', wp_strip_all_tags( $_POST['give_phone'] ) );
	}
}

add_action( 'give_insert_payment', 'myprefix123_give_donations_save_custom_fields', 10, 2 );


/**
 * Show Data in Transaction Details
 *
 * @description show the custom field(s) on the transaction page
 *
 * @param $payment_meta
 * @param $user_info
 */
function give_myprefix_purchase_details( $payment_meta ) {
  //Bounce out if no data for this transaction
  ?>
  <div class="column-container donor-info">
  <?php
  if ( isset( $payment_meta['give_phone'] ) ) {
  ?>
    <div class="column">
      <p>
        <strong>Phone:</strong><br>
        <?php echo $payment_meta['give_phone']; ?>
      </p>
    </div>
  <?php
  }
  if ( isset( $payment_meta['give_yearsatuc'] ) ) {
  ?>
    <div class="column">
      <p>
        <strong>Relationship to UC:</strong><br>
        <?php echo $payment_meta['give_yearsatuc']; ?>
        </p>
    </div>
  <?php
  }
  ?>
  </div>
  <?php
}

add_action( 'give_payment_personal_details_list', 'give_myprefix_purchase_details', 10, 2 );


/**
 * Show Data in Transaction Details
 *
 * @description show the custom field(s) on the transaction page
 *
 * @param $payment_meta
 * @param $user_info
 */
function give_custom__th_after( $payment_id ) {
  ?>
  <?php
}
add_action( 'give_donation_details_thead_after', 'give_custom__th_after', 10, 2 );


/**
 * Show Data in Transaction Details
 *
 * @description show the custom field(s) on the transaction page
 *
 * @param $payment_id
 */
function give_custom__td_after( $payment_id ) {
  $payment_meta   = give_get_payment_meta( $payment_id );
  ?>
  <strong><?php _e( 'Remain anonymous', 'give' ) ?>: </strong>
  <span style="color: red">
  <?php if($payment_meta['give_remain_anonymous'] != "") echo 'YES'; else echo 'NO'; ?>
  </span>

  <br/>
  <br/>

  <strong><?php _e( 'Donation directed to', 'give' ) ?>: </strong>
  <span style="color: red">
  <?php echo $payment_meta['give_direction']; ?>
  </span>

  <?php
  if ( array_key_exists( 'give_specific_direction', $payment_meta ) && ! empty( $payment_meta['give_specific_direction'] ) ) {
    ?>
    <br/>
    <br/>
    <strong><?php _e( 'Specifically', 'give' ) ?>: </strong>
    <span style="color: red">
    <?php echo $payment_meta['give_specific_direction']; ?>
    </span>
    <?php
  }
}
add_action( 'give_donation_details_tbody_after', 'give_custom__td_after', 10, 2 );


/**
 * Show Data in Transaction Details
 *
 * @description show the custom field(s) on the transaction page
 *
 * @param $payment_meta
 * @param $user_info
 */
function new_personal_info_text() {
  return __('Contact Details', 'give');
}
add_filter('give_checkout_personal_info_text', 'new_personal_info_text');



/**
 * Email template tag: direction
 * Destination of the donation
 *
 * @param int $payment_id
 *
 * @return string $direction
 */
function give_direction_tag_donation( $args ) {
  $payment_meta = give_get_payment_meta( $args['payment_id'] );
  return $payment_meta['give_direction'];
}


/**
 * Email template tag: specific_direction
 * Specific destination of the donation
 *
 * @param int $payment_id
 *
 * @return string $specific_direction
 */
function give_specific_direction_tag_donation( $args ) {
  $payment_meta = give_get_payment_meta( $args['payment_id'] );
  return $payment_meta['give_specific_direction'];
}


/**
 * Email template tag: remain_anonymous
 * If the user chose stay anonymous
 *
 * @param int $payment_id
 *
 * @return string $remain_anonymous
 */
function give_remain_anonymous_tag_donation( $args ) {
  $payment_meta = give_get_payment_meta( $args['payment_id'] );
  return $payment_meta['give_remain_anonymous'];
}




/**
 * Add custom tags in the EMAIL template
 */
if( function_exists('give_add_email_tag') ) {
  // Setup default tags array
  $email_tags = array(
    array(
      'tag'         => 'direction',
      'description' => __( 'The name of destination where the donation was chosen if applicable.', 'give' ),
      'function'    => 'give_direction_tag_donation'
    ),
    array(
      'tag'         => 'anonymous',
      'description' => __( 'Whether the user chose stay anonymous or not.', 'give' ),
      'function'    => 'give_remain_anonymous_tag_donation'
    )
  );

  // Add email tags
  foreach ( $email_tags as $email_tag ) {
    give_add_email_tag( $email_tag['tag'], $email_tag['description'], $email_tag['function'] );
  }
}



/**
 * Give Form Require Fields
 *
 * @description: Cusomize the prefix of this function before using. The $required_fields array key is the name of the field you would like to make required; ie " name='card_address_2' " - find using Debug Tools, etc.
 *
 * @param $required_fields
 *
 * @return mixed
 */
function xmycustomprefix_give_form_required_fields( $required_fields ) {

  //Last Name
    $required_fields['give_last'] = array(
        'error_id' => 'invalid_last_name',
        'error_message' => __( 'Please enter your last name.', 'give' )
    );

    return $required_fields;
}
add_filter( 'give_purchase_form_required_fields', 'xmycustomprefix_give_form_required_fields' );



function my_give_payment_receipt_after($payment){
  $output = '';
  $output .= '<tr>';
  $output .= '<td><strong>' .  _e( 'Donation To', 'give' ) . ':</strong></td>';
  $output .= '<td>' . give_get_payment_meta( $payment->ID )["give_direction"] . '</td>';
  $output .= '</tr>';
  if ( array_key_exists( "give_specific_direction", $payment->ID ) && ! empty( give_get_payment_meta( $payment->ID )["give_specific_direction"] ) ) {
      $output .= '<tr>';
      $output .= '<td><strong>' .  _e( 'Specifically', 'give' ) . ':</strong></td>';
      $output .= '<td>' . give_get_payment_meta( $payment->ID )["give_specific_direction"] . '</td>';
      $output .= '</tr>';
  }
  return $output;
}
add_filter( 'give_payment_receipt_after', 'my_give_payment_receipt_after' );


/**
 * Move Billing fields to before Credit Card fields
 */
remove_action( 'give_after_cc_fields', 'give_default_cc_address_fields' );
add_action( 'give_before_cc_fields', 'give_default_cc_address_fields', 10 );





add_filter( 'give_form_title', 'rdln_edit_give_form_title', 10 );
function rdln_edit_give_form_title( $title ) {
    $donation_form_id = 782;
    $title = '<h1 class="give-form-title">' . get_the_title( $donation_form_id ) . '</h1>';
    return $title;
} 
