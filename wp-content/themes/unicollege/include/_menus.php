<?php
/**
 * Hubink functions and definitions
 *
 * Set up the functions for the theme's menu.
 *
 * @package WordPress
 * @subpackage Hubink
 */


/**
 * Custom Template for the quick menu
 */
function get_quick_menu($cclass){
	$menu_name = 'quick_menu';
	$menu_list = '';
	$template_link = '<a class="%s" href="%s"><span class="lnr %s"></span> %s</a>';
	$template_container = '<div class="%s search-container"><span class="lnr %s"></span><a href="#" class="search-title">%s</a> %s</div>';
	$search = '<form role="search" method="get" class="search-form" action="' . get_site_url() . '">
		<div class="form-group">
			<input type="text" value="' . $_GET['s'] . '" name="s" class="search-input">
			<label class="control-label">Enter your search</label>
			<i class="bar"></i>
			<!-- <input type="submit" id="searchsubmit" value="Search"> -->
		</div>
	</form>';

	if ( ( $locations = get_nav_menu_locations() ) && isset( $locations[ $menu_name ] ) ) {
		$menu = wp_get_nav_menu_object( $locations[ $menu_name ] );
		$menu_items = wp_get_nav_menu_items($menu->term_id);
		foreach ( (array) $menu_items as $key => $menu_item ) {
				 $title = $menu_item->title;
				 $classes = implode(' ', $menu_item->classes);
				 $url = $menu_item->url;
				 if($menu_item->type == 'search'){
					 $menu_list .= sprintf($template_container, $cclass, $classes, $title, $search);
				 } else {
					 $menu_list .= sprintf($template_link, $cclass, $url, $classes, $title);
				 }
		 }
	 }
	 return $menu_list;
}


/**
 * Custom Template for the quick menu
 */
function get_social_media_menu($template){
	$menu_name = 'social_media_menu';
	$menu_list = '';
	if ( ( $locations = get_nav_menu_locations() ) && isset( $locations[ $menu_name ] ) ) {
		$menu = wp_get_nav_menu_object( $locations[ $menu_name ] );
		$menu_items = wp_get_nav_menu_items($menu->term_id);
		foreach ( (array) $menu_items as $key => $menu_item ) {
				 $title = $menu_item->title;
				 $url = $menu_item->url;
				 $classes = implode(' ', $menu_item->classes);
				 $menu_list .= sprintf($template, $url, $title, $classes);
		 }
	 }
	 return $menu_list;
}


/**
 * Quick menu for desktop
 */
function the_quick_menu_desktop (){
	echo get_quick_menu('nav-item');

}


/**
 * Quick menu for mobile
 */
function the_quick_menu_mobile(){
	echo get_quick_menu('column');
}


/**
 * Social media menu for mobile
 */
function the_social_media_mobile(){
	echo get_social_media_menu('<a href="%s" title="%s" target="_blank"><i class="fa %s" aria-hidden="true"></i></a>');
}


/**
 * The footer menu
 */
function the_footer_menu(){
	$menu_list = '<div class="column is-half no-print">';
	$menu_list .= '<div id="footer-menu" class="columns is-multiline is-mobile is-gapless">';

	$menu_name = 'footer_menu';
	if ( ( $locations = get_nav_menu_locations() ) && isset( $locations[ $menu_name ] ) ) {
		$menu = wp_get_nav_menu_object( $locations[ $menu_name ] );
		$menu_items = wp_get_nav_menu_items($menu->term_id);
		foreach ( (array) $menu_items as $key => $menu_item ) {
				 $title = $menu_item->title;
				 $url = $menu_item->url;
				 $menu_list .= '<div class="column is-half"><hr class="is-marginless"/>';
				 $menu_list .= sprintf('<a href="%s">%s</a>', $url, $title);
				 $menu_list .= '</div>';
		 }
		 $menu_list .= '</div>';
		 $menu_list .= '</div>';

		 echo $menu_list;
	 }
}


/**
 * Print the mobile versions of the primary menu
 */
function print_primary_menu_mobile($menu_items, $parent) {
	$menu_list = '';
	foreach( $menu_items as $menu_item ) {
		if($menu_item->menu_item_parent != $parent)
			continue;

		$link = $menu_item->url;
		$title = $menu_item->title;
		$classes = implode(' ', $menu_item->classes);
		$children = print_primary_menu_mobile($menu_items, $menu_item->ID);
		$is_parent = !!$children;
		if($classes){
			$icon = '<span class="lnr ' . $classes . '"></span> ';
		} else {
			$icon = '';
		}
		if($menu_item->object_id == get_the_ID()){
			$active = 'is-active';
		} else {
			$active = '';
		}

		if($is_parent){
			$menu_list .= '<div class="menu-drop">';
			// $menu_list .= '<div class="columns is-mobile">';
			$menu_list .= '<a href="' . $link . '" class="nav-item parent primary ' . $active . '">' . $icon . $title . '</a>';
			// $menu_list .= '<div class="column"><a href="' . $url . '" class="nav-item">' . $icon . $title . '</a></div>';
			$menu_list .= '<a class="parent-icon" data-parent="' . $menu_item->ID . '" href="#" aria-label="Open ' . title . ' menu"><span class="icon is-small"><i class="fa fa-chevron-down" aria-hidden="true" ></i></span></a>';
			$menu_list .= '<div class="children" data-parent="' . $menu_item->ID . '">';
			$menu_list .= $children;
			$menu_list .= '</div>';
			$menu_list .= '</div>';
		} else {
			$menu_list .= '<a class="nav-item primary ' . $active . '" href="' . $link . '">' . $icon . $title . '</a>';
		}
	}
	return $menu_list;
}


function &searchMenu(&$menu, $parent_id){
	if($parent_id === '0') return $menu;
	foreach ($menu as &$value) {
		if($value['id'] == intval($parent_id)) {
			return $value['children'];
		} else {
			$c = &searchMenu($value['children'], $parent_id);
			if($c !== null){
				return $c;
			}
		}
	}

	return null;
}

function addMenu(&$menu, $item) {
	$parent = &searchMenu($menu, $item->menu_item_parent);
	if($parent === null) $parent = &$menu;

	array_push($parent, array(
		'id' => $item->ID,
		'parent' => $item->menu_item_parent,
		'title' => $item->title,
		'classes' => $item->classes,
		'object_id' => $item->object_id,
		'url' => $item->url,
		'children' => array(),
	));
}

function createMenu($menu_items) {
	$newmenu = array();

	foreach ($menu_items as $value) {
		addMenu($newmenu, $value);
	}

	return $newmenu;
}

/**
 * Print the desktop versions of the primary menu
 */
function clean_custom_menu( $theme_location ) {
		if ( ($theme_location) && ($locations = get_nav_menu_locations()) && isset($locations[$theme_location]) ) {
				$menu = get_term( $locations[$theme_location], 'nav_menu' );
				$menu_items = wp_get_nav_menu_items($menu->term_id);
				$menu_items = createMenu($menu_items);
				$menu_list = '<div id="main-menu-desktop" class="columns is-mobile">' ."\n";
				$count = 0;
				$submenu = false;

				// create the Level 1
				foreach( $menu_items as $l1 ) {
					if($l1['object_id'] == get_the_ID()){
						$active = 'is-active';
					} else {
						$active = '';
					}

					$arrow = true;
					foreach ( $l1['classes'] as $class ) {
						if ( $class === 'no-arrow' ) {
							$arrow = false;
						}
					}

					$classes = implode(' ', $l1['classes']);
					$menu_list .= '<div class="column is-narrow ' . $classes . '">' ."\n";
					$menu_list .= '<a href="'.$l1['url'].'" class="' . $active . '">' . strtoupper($l1['title']) . ( $arrow ? ' <i class="fa fa-chevron-down" aria-hidden="true"></i>' : '' ) . '</a>' ."\n";

					// create the Level 2
					if(count($l1['children'])){
						$menu_list .= '<div class="submenu">' ."\n";
						$menu_list .= '<a href="'.$l1['url'].'" class="h1">' . $l1['title'] . '</a>' ."\n";
						$menu_list .= '<hr class="big"/>' ."\n";
						$menu_list .= '<div class="columns is-multiline is-mobile">' ."\n";
						foreach( $l1['children'] as $l2 ) {
							if($l2['object_id'] == get_the_ID()){
								$active = 'is-active';
							} else {
								$active = '';
							}

							// Check if there is a Level 3
							if(count($l2['children']) == 0){

								$menu_list .= '<div class="column is-4">' ."\n";
								$menu_list .= '<a href="'.$l2['url'].'" class="'.$active.'">'.$l2['title'].'</a>' ."\n";
								$menu_list .= '<hr/>' ."\n";
								$menu_list .= '</div>' ."\n";

							}else{
								$menu_list .= '<div class="column second-level is-12">' ."\n";
								$menu_list .= '<a href="'.$l2['url'].'" class="'.$active.'">'.$l2['title'].'</a>' ."\n";
								$menu_list .= '<hr/>' ."\n";
								$menu_list .= '</div>' ."\n";

								// create the Level 3
								foreach( $l2['children'] as $l3 ) {
									if($l3['object_id'] == get_the_ID()){
										$active = 'is-active';
									} else {
										$active = '';
									}


									$menu_list .= '<div class="column is-4">' ."\n";
									$menu_list .= '<a href="'.$l3['url'].'" class="'.$active.'">'.$l3['title'].'</a>' ."\n";
									$menu_list .= '<hr/>' ."\n";
									$menu_list .= '</div>' ."\n";
								}
							}
						}
						$menu_list .= '</div>' ."\n";
						$menu_list .= '</div>' ."\n";
					}

					$menu_list .= '</div>' ."\n";
				}
				$menu_list .= '</div>' ."\n";

		} else {
				$menu_list = '<!-- no menu defined in location "'.$theme_location.'" -->';
		}
		echo $menu_list;
}

?>