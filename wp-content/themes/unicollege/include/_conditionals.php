<?php 

function is_events_related_page() {
  if (!function_exists('em_is_events_page')) return false;

  return (
    em_is_events_page()
    || em_is_event_page()
    || em_is_calendar_day_page()
    || em_is_category_page()
    || em_is_categories_page()
    || em_is_tag_page()
    || em_is_location_page()
    || em_is_locations_page()
    || em_is_my_bookings_page()
  );
}
