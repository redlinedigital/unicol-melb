<?php
/**
 * SecurePay Standard Gateway
 *
 * @package     Give
 * @subpackage  Gateways
 * @copyright   Copyright (c) 2016, WordImpress
 * @license     http://opensource.org/licenses/gpl-2.0.php GNU Public License
 * @since       1.0
 */

define('SECUREPAY_PREFIX', '_give_securepay_');

require 'securepay_xml_api.php';


/**
 * Register the payment gateway
 *
 * @since  1.0
 *
 * @param array $gateways
 *
 * @return array
 */
function give_securepay_register_gateway( $gateways ) {
	// Format: ID => Name
	$gateways['securepay'] = array(
		'admin_label'    => __( 'SecurePay Standard', 'give' ),
		'checkout_label' => __( 'SecurePay', 'give' ),
		'supports'       => array( 'buy_now' )
	);

	return $gateways;
}

add_filter( 'give_payment_gateways', 'give_securepay_register_gateway', 1 );


/**
 * Process SecurePay Purchase
 *
 * @since 1.0
 *
 * @param array $purchase_data Purchase Data
 *
 * @return void
 */
function give_process_securepay_purchase( $purchase_data ) {
	define('COMET_CACHE_ALLOWED', FALSE);
	
	if ( ! wp_verify_nonce( $purchase_data['gateway_nonce'], 'give-gateway' ) ) {
		wp_die( __( 'Nonce verification has failed', 'give' ), __( 'Error', 'give' ), array( 'response' => 403 ) );
	}

	$form_id = intval( $purchase_data['post_data']['give-form-id'] );

	// Collect payment data
	$payment_data = array(
		'price'           => $purchase_data['price'],
		'give_form_title' => $purchase_data['post_data']['give-form-title'],
		'give_form_id'    => $form_id,
		'date'            => $purchase_data['date'],
		'user_email'      => $purchase_data['user_email'],
		'purchase_key'    => $purchase_data['purchase_key'],
		'currency'        => give_get_currency(),
		'user_info'       => $purchase_data['user_info'],
		'status'          => 'pending',
		'gateway'         => 'securepay'
	);

	// Record the pending payment
	$payment = give_insert_payment( $payment_data );

	// Check payment
	if ( ! $payment ) {
		// Record the error
		give_record_gateway_error( __( 'Payment Error', 'give' ), sprintf( __( 'Payment creation failed before sending buyer to SecurePay. Payment data: %s', 'give' ), json_encode( $payment_data ) ), $payment );
		// Problems? send back
		give_send_back_to_checkout( '?payment-mode=' . $purchase_data['post_data']['give-gateway'] );
	} else {

		// Setup the api
		if ( give_is_test_mode() ) {
			// Test mode
			$payment_mode = SECUREPAY_GATEWAY_MODE_TEST;
		} else {
			// Live mode
			$payment_mode = SECUREPAY_GATEWAY_MODE_LIVE;
		}

		$merchant_id = give_get_option( SECUREPAY_PREFIX . 'merchant_id', false );
		$transaction_password = give_get_option( SECUREPAY_PREFIX . 'transaction_password', false );


		//Item name - pass level name if variable priced
		$item_name = $purchase_data['post_data']['give-form-title'];
		$txtReferenc = $payment;

		$securepay_api = new securepay_xml_transaction($payment_mode, $merchant_id, $transaction_password, '');
		$banktxnID = $securepay_api->processCreditStandard(
			$purchase_data['price'],
			$txtReferenc,
			$purchase_data['card_info']['card_number'],
			$purchase_data['card_info']['card_exp_month'],
			$purchase_data['card_info']['card_exp_year'],
			$purchase_data['card_info']['card_cvc'],
			give_get_currency()
		);
		if($banktxnID){
			give_set_payment_transaction_id($payment, $banktxnID);
			give_update_payment_status($payment, 'complete');
			give_send_to_success_page();
		} else {
			give_update_payment_status($payment, 'failed');
			give_insert_payment_note($payment, 'ERROR: ' . $securepay_api->getErrorString());
			give_set_error( '', $securepay_api->getErrorString() );
			give_record_gateway_error( __( 'Payment Error', 'give' ), sprintf( __( 'Payment creation failed before sending buyer to SecurePay. Payment data: %s', 'give' ), $securepay_api->getErrorString() ), $payment );
			give_send_back_to_checkout( '?payment-mode=securepay' );
		}
	}

}

add_action( 'give_gateway_securepay', 'give_process_securepay_purchase' );



/**
 * SecurePay Success Page
 *
 * @description: Shows "Donation Processing" message for SecurePay payments that are still pending on site return
 *
 * @since      1.0
 *
 * @param $content
 *
 * @return string
 *
 */
function give_securepay_success_page_content( $content ) {

	if ( ! isset( $_GET['payment-id'] ) && ! give_get_purchase_session() ) {
		return $content;
	}

	$payment_id = isset( $_GET['payment-id'] ) ? absint( $_GET['payment-id'] ) : false;

	if ( ! $payment_id ) {
		$session    = give_get_purchase_session();
		$payment_id = give_get_purchase_id_by_key( $session['purchase_key'] );
	}

	$payment = get_post( $payment_id );

	if ( $payment && 'pending' == $payment->post_status ) {

		// Payment is still pending so show processing indicator to fix the Race Condition
		ob_start();

		give_get_template_part( 'payment', 'processing' );

		$content = ob_get_clean();

	}

	return $content;

}

add_filter( 'give_payment_confirm_securepay', 'give_securepay_success_page_content' );

/**
 * Register gateway settings
 *
 * @since  1.0
 * @return array
 */
function give_securepay_add_settings( $settings ) {
	$is_gateway_active = give_is_gateway_active( 'securepay' );

	//this gateway isn't active
	if ( ! $is_gateway_active ) {
		//return settings and bounce
		return $settings;
	}

	//Fields
	$check_settings = array(

		array(
			'name' => __( 'SecuryPay Standard', 'give' ),
			'desc' => '',
			'type' => 'give_title',
			'id'   => 'give_title_gateway_settings_99',
		),
		array(
			'id'          => SECUREPAY_PREFIX . 'merchant_id',
			'name'        => esc_attr__( 'Merchant ID', 'give' ),
			'desc'        => esc_attr__( 'Enter your NAB Transact account\'s Merchant ID.', 'give' ),
			'default'     => '',
			'type'        => 'text',
			'row_classes' => 'give-subfield',
		),
		array(
			'id'          => SECUREPAY_PREFIX . 'transaction_password',
			'name'        => esc_attr__( 'Transaction Password', 'give' ),
			'default'     => '',
			'type'        => 'text',
			'row_classes' => 'give-subfield',
		),
	);

	return array_merge( $settings, $check_settings );
}

add_filter( 'give_settings_gateways', 'give_securepay_add_settings' );
