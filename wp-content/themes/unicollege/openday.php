<?php
/*
 * Template Name: Open Day Page
 */
?>

<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other 'pages' on your WordPress site will use a different template.
 *
 * @package WordPress
 */

get_header(); ?>
<div class="content-wrap">
	<div class="content">
		<?php
		get_template_part( 'breadcrumb' );
		?>
		<div class="openday-banner"></div>
		<section class="main content-page">
			<?php

				while ( have_posts() ) : the_post();
					get_template_part( 'content', 'openday' );
				endwhile; // end of the loop.
			?>
		</section>
		<aside class="sidebar aside">
			<?php get_sidebar(); ?>
		</aside>
	</div><!-- End .content -->
</div><!-- End .content-wrap -->
<div id="contact_map"></div>
<script>
  function initMap() {
    var location = {lat: -37.7920484, lng: 144.9597386};
    var map = new google.maps.Map(document.getElementById('contact_map'), {
      zoom: 15,
      center: location
    });
    var contentString = '<div style="padding: 6px; text-align: center;">'+
	    '<h4>University College</h4>'+
	    '<div>'+
	    '<p>40 College Cres, Parkville VIC 3052</p>' +
	    '</div>'+
	    '</div>';
  	var infowindow = new google.maps.InfoWindow({
      content: contentString,
      maxWidth: 400
    });
    var marker = new google.maps.Marker({
      position: location,
      map: map
    });
    marker.addListener('click', function() {
      infowindow.open(map, marker);
    });
    infowindow.open(map, marker);
  }
</script>
<style>
	#contact_map {
		width: 100%;
		height: 30rem;
		display: block;
	}
	.openday-banner {
		width: 100%;
		height: 200px;
		background-image: url(<?php echo IMG; ?>/openday.jpg);
		background-size: cover;
		background-position: center;
	}
	.content-page .entry-header h1 {
		margin-bottom: 20px !important;
	}
	.content h2 {
		font-size: 24px !important;
		line-height: 30px !important;
	}
	.content h3 {
		font-size: 18px !important;
		line-height: 24px !important;
		color: black !important;
		margin-top: 20px !important;
	}
	@media screen and (min-width: 680px) {
		.openday-banner {
			height: 400px;
		}
		.content h2 {
			font-size: 34px !important;
			line-height: 40px !important;
		}
		.content h3 {
			font-size: 22px !important;
			line-height: 28px !important;
		}
	}
</style>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyApaDVz7GFeBxb9q8N4loG-YyPh21swxac&callback=initMap"></script>
<?php get_footer(); ?>
