<?php
/**
 * The template for displaying Search Results pages
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

<div class="content-wrap">
	<div class="content">
		<section class="content">
			<div class="container content-page">
				<div class="columns">
					<div class="column">
						<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
							<?php if ( have_posts() ) : ?>

							<header class="page-header">
								<h1 class="page-title"><?php printf('Search Results for: %s', get_search_query() ); ?></h1>
							</header><!-- .page-header -->

									<?php
									// Start the loop.
									while ( have_posts() ) : the_post();

										/**
										 * Run the loop for the search to output the results.
										 * If you want to overload this in a child theme then include a file
										 * called content-search.php and that will be used instead.
										 */
									  echo '<div class="students">';
										get_template_part( 'intro', 'post' );
										echo '</div>';

									// End the loop.
									endwhile;

									// Previous/next page navigation.
									// the_posts_pagination( array(
									// 	'prev_text'          => 'Previous page',
									// 	'next_text'          => 'Next page'
									// ) );
								?>

								<?php
								$prev_link = get_previous_posts_page_link();
								$next_link = get_next_posts_page_link();
								?>


								<nav class="pagination">
									<?php if($paged > 1): ?>
										<a href="<?php echo $prev_link; ?>" class="button">Go back</a>
									<?php else: ?>
										<!-- <a class="button is-disabled">Newer posts</a> -->
									<?php endif; ?>

									<?php if($wp_query->max_num_pages > $paged): ?>
										<a href="<?php echo $next_link; ?>" class="button">View more</a>
									<?php else: ?>
										<!-- <a class="button is-disabled">Older posts</a> -->
									<?php endif; ?>
								</nav>

								<?php


								else :
									// If no content, include the "No posts found" template.
									get_template_part( 'content', 'none' );

								endif;
							?>

						</article><!-- #post-<?php the_ID(); ?> -->
					</div>
				</div>
			</div>
		</section>
	</div>
</div>

<?php
get_footer();
