<?php
/*
Template Name: Venues
*/
get_header(); 

wp_enqueue_style( 'flexslider', get_template_directory_uri() . '/js/vendor/flexslider.css');
wp_enqueue_script( 'script', get_template_directory_uri() . '/js/vendor/jquery.flexslider-min.js', array ( 'jquery' ), 1.1, true);


function icon_venues($title, $number) {
  ?>
  <li title="<?php echo $title; ?>">
    <img src="<?php echo IMG . '/venues/' . str_replace('-','',strtolower($title)) . '.svg'; ?>" alt="<?php echo $title; ?>">
    <span><?php echo $number ? $number : 'N/A'; ?></span>
  </li>
  <?php
}

?>
<script type="text/javascript" charset="utf-8">
  jQuery(window).load(function() {
    jQuery('.flexslider').flexslider({
      animation: 'slide',
      controlNav: false
    });
  });
</script>

<div class="content-wrap venues">
	<div class="content">
		<?php
		get_template_part( 'breadcrumb' );
		?>

	</div><!-- End .content -->

  <div class="container">
      <?php 
      $images = get_field('carousel');
      if( $images ): ?>
      <div class="flexslider">
          <ul class="slides">
            <?php foreach( $images as $image ): ?>
              <li>
                <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
              </li>
            <?php endforeach; ?>
          </ul>
      </div>
      <?php endif; ?>
  </div>

	<div class="content">
    <section class="main content-page">
      <div class="container">
        <h1 class="entry-title museo-slab-300 blue has-text-centered"><?php the_title(); ?></h1>
        <div class="entry-content">
          <?php the_content(); ?>
        </div>

        <ul class="caption">
          <li><img src="<?php echo IMG . '/venues/dining.svg'; ?>" alt="Dining"><strong>Dining</strong></li>
          <li><img src="<?php echo IMG . '/venues/classroom.svg'; ?>" alt="Classroom"><strong>Classroom</strong></li>
          <li><img src="<?php echo IMG . '/venues/boardroom.svg'; ?>" alt="Boardroom"><strong>Boardroom</strong></li>
          <li><img src="<?php echo IMG . '/venues/standing.svg'; ?>" alt="Standing"><strong>Standing</strong></li>
          <li><img src="<?php echo IMG . '/venues/theatre.svg'; ?>" alt="Theatre"><strong>Theatre</strong></li>
          <li><img src="<?php echo IMG . '/venues/ushaped.svg'; ?>" alt="U-Shaped"><strong>U-Shaped</strong></li>
        </ul>
      </div>
		</section>
    <hr class="separator" />
      <?php 
      $venues = get_field('venues');
      if( $venues ): ?>
    <section class="main content-page">
      <div class="container venues-list">
            <?php foreach( $venues as $venue ): ?>
              <div class="venues-list-item">
                <h3><?php echo $venue['venue_name']; ?></h3>

                <div class="venues-list-item-details">
                  <?php 
                    if( $venue['venue_image'] ) {
                      echo wp_get_attachment_image( $venue['venue_image'], array(390, 295) );
                    }
                  ?>
                  
                  <div class="venues-list-item-right">
                    <?php echo $venue['venue_description']; ?>
                    <ul class="caption small">
                    <?php 
                    icon_venues("Dining", $venue['venue_space']['dining']);
                    icon_venues("Classroom", $venue['venue_space']['classroom']);
                    icon_venues("Boardroom", $venue['venue_space']['boardroom']);
                    icon_venues("Standing", $venue['venue_space']['standing']);
                    icon_venues("Theatre", $venue['venue_space']['theatre']);
                    icon_venues("U-Shaped", $venue['venue_space']['ushaped']);
                    ?>
                    </ul>
                  </div>
                  
                  <div>
                  <?php 
                    if( $venue['venue_virtual_tour'] ) {
                      echo "<h6>" . $venue['venue_name'] . " Virtual Tour</h6>";
                      echo $venue['venue_virtual_tour'];
                    }
                  ?>  
                  </div>

                </div>
                              
              </div>
            <?php endforeach; ?>
      </div>
    </section>

    <section class="main content-page">
      <div class="container">
        <?php the_field('after_content'); ?>
      </div>
		</section>
    <?php endif; ?>
</div><!-- End .content-wrap -->
<?php get_footer(); ?>
