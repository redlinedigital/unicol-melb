<?php
/*
Template Name: Two blocks
*/
get_header(); ?>
<div class="content-wrap">
	<div class="content">
		<?php
		get_template_part( 'breadcrumb' );
		?>

    <?php
    // check if the flexible content field has rows of data
    if( have_rows('blocks') ):
        $count = 0;
    ?>
    <div class="blocks full-columns is-gapless is-fullpage">
      <?php
         // loop through the rows of data
        while ( have_rows('blocks') ) : the_row();
          $photo = get_sub_field('photo');
            if( get_row_layout() == 'left_block' ):
            ?>
        			<div class="full-column right">
                <figure>
                  <img src="<?php echo $photo['url']; ?>" alt="<?php echo $photo['alt'] ?>" />
                  <figcaption class="wp-caption-text">
                    <?php
                    echo ( $count == 0 ? '<h1 class="title">' : '<span class="title">' );
                    the_sub_field( 'title' );
                    echo ( $count == 0 ? '</h1>' : '</span>' );
                    ?>
                    <span class="description"><?php the_sub_field('description'); ?></span>
                    <a href="<?php the_sub_field('page'); ?>" class="button is-transparent is-transparent-90">
                      Discover more <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
										</a>
                  </figcaption>
                </figure>
        			</div>
            <?php
            elseif( get_row_layout() == 'right_block' ):
            ?>
        			<div class="full-column left">
                <figure>
                  <img src="<?php echo $photo['url']; ?>" alt="<?php echo $photo['alt'] ?>" />
                  <figcaption class="wp-caption-text">
                    <span class="title"><?php the_sub_field('title'); ?></span>
                    <span class="description"><?php the_sub_field('description'); ?></span>
                    <a href="<?php the_sub_field('page'); ?>" class="button is-transparent is-transparent-90">
                      Discover more <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
										</a>
                  </figcaption>
                </figure>
        			</div>
            <?php
            endif;
            $count++;
        endwhile;
        ?>
    </div>
    <?php
    endif;
    ?>

	</div>
</div><!-- End .content-wrap -->
<?php get_footer(); ?>
