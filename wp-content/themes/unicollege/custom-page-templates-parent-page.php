<?php
/*
Template Name: Parent Page
*/
get_header(); ?>
<div class="content-wrap">
	<div class="content">
		<?php
		get_template_part( 'breadcrumb' );
		?>
		<section class="main content-page">

      <div class="container">
      	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    			<header class="entry-header">
    				<h1 class="entry-title museo-slab-300 blue"><?php the_title(); ?></h1>
    			</header>

      		<div class="entry-content">
      			<?php echo $post->post_content; ?>

      			<div class="columns is-multiline">
      			<?php
      				$posts = get_children(array(
      					'post_parent' => $post->ID,
      					'post_type' => 'page',
      					'post_status' => 'publish',
      					'posts_per_page' => -1,
    						'orderby' => array( 'menu_order' => 'ASC' )
      				));
      				foreach ($posts as $post):
      					setup_postdata($post);
      			?>
      					<figure class="image red-style parent-page column is-3">
      						<a href="<?php the_permalink(); ?>">
										<div class="overlay">
											<?php the_post_thumbnail('parent-page'); ?>
										</div>
	      						<figcaption class="wp-caption-text">
											<span><?php the_title(); ?></span>
										</figcaption>
	      					</a>
								</figure>
      			<?php endforeach; ?>
      			</div>
      		</div><!-- .entry-content -->
      	</article>
      </div>

		</section>
		<aside class="sidebar aside">
			<?php get_sidebar(); ?>
		</aside>
	</div><!-- End .content -->
</div><!-- End .content-wrap -->
<?php get_footer(); ?>
